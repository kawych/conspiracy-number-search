#ifndef __DEBUG_THINGS_H__
#define __DEBUG_THINGS_H__

#ifndef NDEBUG
	const bool debug = true;
#else
	const bool debug = false;
#endif

#define _unused(x) ((void)(x))

#endif
