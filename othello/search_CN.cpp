#include "othello.h"
#include "../CNS/CNsearch.hpp"
#include "../include/debug_things.h"

#ifdef total_iter
const int iterations = total_iter;
#else
const int iterations = 5000;
#endif

// global TT
CNSearch<Othello> solver;

extern "C" int CNS_midgame(Search* search, int dummy, struct Node * parent)
	{
		Othello game(search, parent);
		if(game.endOfGame())
		{
			return game.eval0();
		}
		else
		{
			solver.setGame(&game);
			// local TT
			//CNSearch<Othello> solver(&game);
			CNSearch<Othello>::Tree * tree = solver.newTree(Othello::DRAW_SCORE);
			solver.developNode(tree, 0);
			assert(!game.endOfGame());
			solver.updateNode(tree, 0);

#ifdef DFS
			Othello::EvalT val = solver.evalPositionDFS(tree, iterations, 0);
#else
			Othello::EvalT val = solver.evalPosition(tree, iterations, 0);
#endif
			solver.cleanTree(tree);
			return val;
		}
		_unused(dummy);
	}
