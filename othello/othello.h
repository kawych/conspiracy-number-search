#ifndef __OTHELLO_H__
#define __OTHELLO_H__

#include <stack>
#include <vector>
#include <string>
#include <stdint.h>
#include "../include/debug_things.h"

extern "C" {
#include "search.h"
#include "bit.h"
#include "options.h"
#include "stats.h"
#include "ybwc.h"
#include "settings.h"
#include "move.h"
}

class Othello {
	public:
		typedef struct Move MoveT;
		typedef short EvalT;
		typedef unsigned long long HashT;
		struct MoveListT
		{
			MoveList movelist;
			Node node;
		};

		static const EvalT EPSILON = 1;
		static const EvalT CN_STEP = 1;
		static const EvalT WIN_SCORE = SCORE_MAX;
		static const EvalT DRAW_SCORE = 0;
		static const EvalT INF_SCORE = SCORE_INF; // const.h
		static const EvalT MIN_SCORE = SCORE_MIN; // const.h
		static const EvalT MAX_SCORE = SCORE_MAX; // const.h
		//static const MoveT WRONG_MOVE = -1;
		//static MoveT* PASS_MOVE = &MOVE_PASS;
		//static const MoveT * NULL_MOVE = (MoveT *) NULL;
		static const unsigned int MOVES_PER_PLAYER = 1;

		Othello(Search * search, struct Node * parent);

		void makeMove(MoveT const * move);
		void undoMove();
		void makePass();
		void undoPass();

		MoveListT * getMoveList();
		void rmMoveList(MoveListT *moves);
		MoveT * getFirstMove(MoveListT *moves);
		MoveT * getNextMove(MoveListT *moves);
		int countMoves(MoveListT * moves);
		//unsigned int movesCnt();
		bool canMove();
		void sortMoves(MoveListT *moves);

		bool endOfGame() const;
		EvalT eval();
		EvalT eval0();
		int currentPlayer() const;
		HashT stateHash() const;
		MoveT const * getPass() {
			return &MOVE_PASS;
		}
	private:
		Search *search;
		Node *node;
		Board *board;
		std::stack<MoveT> history;
};

#endif
