#include "othello.h"

#include <vector>
#include <string>
#include <stdint.h>

Othello::Othello(Search * search, struct Node * parent)
{
	this->search = search;
	this->node = parent;
	this->board = search->board;
}

void Othello::makeMove(Othello::MoveT const * move)
{
	history.push(*move);
	search_update_midgame(search, move);
}

void Othello::undoMove()
{
	MoveT move = history.top();
	history.pop();
	search_restore_midgame(search, &move);
}

void Othello::makePass()
{
	search_update_pass_midgame(search);
}

void Othello::undoPass()
{
	search_restore_pass_midgame(search);
}

Othello::MoveListT * Othello::getMoveList()
{
	MoveListT * moveList = new MoveListT();
	search_get_movelist(search, &moveList->movelist);
	node_init(&moveList->node, search, -64, 64, 100,
			moveList->movelist.n_moves, (Node *) NULL); // not sure about this NULL

	return moveList;
}

void Othello::rmMoveList(MoveListT * moveList)
{
	node_free(&moveList->node);
	delete moveList;
}

int Othello::countMoves(MoveListT * moveList)
{
	return moveList->movelist.n_moves;
}

bool Othello::canMove()
{
	return can_move(this->board->player, this->board->opponent);
}

Othello::MoveT * Othello::getFirstMove(MoveListT * moveList)
{
	MoveT * move = node_first_move(&moveList->node, &moveList->movelist);
	return move;
}

Othello::MoveT * Othello::getNextMove(MoveListT * moveList)
{
	MoveT * move = node_next_move(&moveList->node);
	return move;
}


void Othello::sortMoves(MoveListT *moves)
{
	_unused(moves);
}

bool Othello::endOfGame() const
{
	return !can_move(board->player, board->opponent) &&
		!can_move(board->opponent, board->player);
}

Othello::EvalT Othello::eval()
{
#ifndef ab_lv
	return search_eval_0(search);
#elif ab_lv == 0
	return search_eval_0(search);
#elif ab_lv == 1
	return search_eval_1(search, MIN_SCORE, MAX_SCORE);
#elif ab_lv == 2
	return search_eval_2(search, MIN_SCORE, MAX_SCORE);
#else
	return PVS_shallow(search, MIN_SCORE, MAX_SCORE, ab_lv);
#endif
}

Othello::EvalT Othello::eval0()
{
	return search_eval_0(search);
}

Othello::HashT Othello::stateHash() const
{
	return board_get_hash_code(this->board);
}
