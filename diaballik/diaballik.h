#ifndef __DIABALLIK_H__
#define __DIABALLIK_H__

#include <vector>
#include <string>
#include <stdint.h>
#include <cassert>
#include <set>

typedef int Position;
typedef int Direction;
typedef int Piece;

enum { STAY = 0, N = 1, E = 2, S = 3, W = 4, NW = 5, NE = 6, SW = 7, SE = 8 };

const int MOVE_DIRS = 4;
const int THROW_DIRS = 8;
const int MOVES = 2;
const int THROWS = 1;
const int PIECES = 7;
const int SIZE = 81;
const int WHITE = 0, BLACK = 1;

class Diaballik {
	public:
		typedef uint64_t HashT;
		typedef int MoveT;
		typedef float EvalT;
		typedef struct {
			std::vector<MoveT> list;
			unsigned int position;
		} MoveListT;

		struct Params {
			float tempoWeight; // 0.5
			float movesToWinWeight; // 0.5
			float ballMobilityWeight; // 5.
			float piecesMobilityWeight; // 3.

			Params(int argc, char *argv[]);
		};

		static const float EPSILON = 0.000001;
		static const float CN_STEP = 0.1;
		static const float WIN_SCORE = 1000.;
		static const float DRAW_SCORE = 0.;
		static const EvalT INF_SCORE = 10000.;
		static const EvalT MIN_SCORE = -1000.;
		static const EvalT MAX_SCORE = 1000.;
		static const MoveT WRONG_MOVE = -1;
		static const int PARAMS_CNT = 4;
		static const unsigned int MOVES_PER_PLAYER = 6;

		Diaballik(Params params);
		void makeMove(MoveT const move);
		void makeMove(MoveT const * move);
		void makeMove(std::string move);
		void undoMove();
		MoveT const * getPass() {
			assert(false);
			return NULL;
		}
		void makePass();
		void undoPass();
		bool canMove();
		void forceEndTurn();
		MoveListT * getMoveList();
		void rmMoveList(MoveListT *moves);
		MoveT * getFirstMove(MoveListT * moves);
		MoveT * getNextMove(MoveListT * moves);
		unsigned int movesCnt(); // not needed, used only in flat search
		bool endOfGame() const;
		EvalT eval();
		int currentPlayer() const;
		uint64_t stateHash() const;

		std::string moveToStr(const MoveT move);
		std::string printBoard() const;
	private:
		struct Undo {
			Piece piece;
			Position dest;
			bool throwLeft;
			int movesLeft;
			int critCounter;
		};
		Params params;

		std::vector<MoveT> pieces[2];
//		std::vector<MoveT> moves;
		std::vector<Undo> history;

		bool areInLine[SIZE][SIZE];
		int dist[SIZE][SIZE];
		int direction[SIZE][SIZE];
		int closestEnemy[THROW_DIRS];
		int board[SIZE];
		int we;
		int position[2 * PIECES];
		int ball[2];
		int contactPoints;
		Piece currentPiece;
		bool pieceChosen;
		bool throwLeft;
		int movesLeft;
		int actionsDone;

		int bfsBallHelper[SIZE];
		int bfsPiece1Helper[SIZE];
		int bfsPiece2Helper[SIZE];
		Piece bfsClosest1Piece[SIZE];
		Piece bfsClosest2Piece[SIZE];
		float activity[2 * PIECES];
		float ballMobility;
		float piecesMobility;

		uint64_t hashTable[SIZE][5];
		uint64_t hashPlayer;
		uint64_t hashThrow[2];
		uint64_t hashMoves[3];
		uint64_t hashActions[4];
		uint64_t hashPieceChosen;
		uint64_t hashValue;
		int critCounter;

		std::set<uint64_t> forbiddenStates;

		void initHash();

		int enemy(int player) const;
		//void generateMoves();
		void execute(MoveT move);
		bool contactsEnemy(Piece piece) const;
		Piece nextPiece(Piece p) const;
		bool hasLine(int player) const;
		bool owns(int player, Piece piece) const;

		void piecesBfs(int player);
		void ballBfs(int player);
		int movesToContact(int player);
		int movesToReachEnd(int player);
		float movesToWin(int player);
};

inline int Diaballik::currentPlayer() const {
	return we;
}

inline int Diaballik::enemy(int player) const {
	return 1 - player;
}

inline bool Diaballik::owns(int player, Piece piece) const {
	return piece >= player * PIECES && piece < (player + 1) * PIECES;
}

inline uint64_t Diaballik::stateHash() const {
	return hashValue;
}

#endif
