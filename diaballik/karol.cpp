#include <iostream>
#include <string>
#include <cassert>
#include "diaballik.h"
#include "../CNS/CNsearch.hpp"

using namespace std;

#ifdef total_iter
const int CNS_ITERATIONS = total_iter;
#else
const int CNS_ITERATIONS = 8000;
#endif

int main(int argc, char *argv[]) {
	Diaballik::Params params(argc, argv);
	Diaballik game(params);
	CNSearch<Diaballik> cnsearch(&game);
	int time;

	while(true) {
		string command;
		cin >> command;
		if(command == "time_left") {
			cin >> time;
			cout << "=" << endl;
			fflush(stdout);
		}
		else if(command == "play") {
			cnsearch.checkpoint();
			string move, player;
			cin >> player >> move;
			if(move.length() >= 4) {
				game.makeMove(move.substr(0, 4));
			}
			if(move.length() >= 8) {
				game.makeMove(move.substr(4, 4));
			}
			if(move.length() >= 12) {
				game.makeMove(move.substr(8, 4));
			}
			if(move.length() < 12) {
				game.forceEndTurn();
			}
			cout << "=" << endl;
			fflush(stdout);
		}
		else if(command == "gen_move") {
			string player;
			cin >> player;
			Diaballik::MoveT* moves;
			moves = cnsearch.bestMove(CNS_ITERATIONS);
			cout << "= ";
			for(int i = 0; i < 6; i += 2) {
				if(moves[i] != moves[i+1] && moves[i] != -1) {
					cout << game.moveToStr(moves[i]) << game.moveToStr(moves[i+1]);
				}
			}
			cout << endl;
			fflush(stdout);
		}
		else if(command == "print_board")
			cout << game.printBoard();
		else if(command == "quit")
			break;
	}

	return 0;
}
