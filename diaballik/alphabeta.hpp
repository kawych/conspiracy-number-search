#ifndef __ALPHABETA_H__
#define __ALPHABETA_H__

#include <cstdlib>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <stdint.h>
#include <set>
#include "transposition_table.hpp"

using namespace std;

const unsigned int MOVES_PER_PLAYER = 6;
const float EPS = 0.001;

template <class Game>
class AlphaBeta {
	public:
		AlphaBeta(Game& game);

		typename Game::Move* bestMove(int depth_);
		void checkpoint();
	private:
		int depth;
		Game& game;
		set<uint64_t> history;
		typename Game::Move moves[MOVES_PER_PLAYER];
		TranspositionTable hashTable;

		float alphabeta(uint32_t d, float alpha, float beta);
};

template <class Game>
AlphaBeta<Game>::AlphaBeta(Game& g) : game(g) { }

template <class Game>
inline void AlphaBeta<Game>::checkpoint() {
	history.insert(game.stateHash());
}

template <class Game>
typename Game::Move* AlphaBeta<Game>::bestMove(int depth_) {
	for(unsigned int i = 0; i < MOVES_PER_PLAYER; ++i)
		moves[i] = Game::WRONG_MOVE;
	for(depth = depth_;
			alphabeta(0, -Game::WIN_SCORE, Game::WIN_SCORE) == -Game::WIN_SCORE;
			depth -= (depth % 2 == 0) ? 1 : 2) {
		hashTable.clear();
		assert(depth > 0);
	}
	return moves;
}

template <class Game>
float AlphaBeta<Game>::alphabeta(uint32_t d, float alpha, float beta) {
	assert(alpha < beta);
	assert(d <= MOVES_PER_PLAYER * depth);

	if(d % MOVES_PER_PLAYER == 0 && history.find(game.stateHash()) != history.end()) {
		assert(!game.endOfGame());
		return Game::DRAW_SCORE;
	}

	// check transposition table for current state
	if((d % 2) == 0) {
		int idx = hashTable.find(game.stateHash());
		if(idx >= 0) {
			assert(!game.endOfGame());
			if(hashTable.depth(idx) <= d) {
				float l = hashTable.lower(idx), u = hashTable.upper(idx);
				if(u <= alpha) {
					return u;
				}
				else if(l >= beta) {
					if(d < MOVES_PER_PLAYER) {
						assert(l == beta);
						moves[d] = hashTable.move(idx);
						assert(moves[d] != Game::WRONG_MOVE);
						if(d < MOVES_PER_PLAYER - 1) {
							game.makeMove(moves[d]);
							alphabeta(d + 1, alpha, beta);
							game.undoMove();
						}
					}
					return l;
				}
				else if(l == u) {
					if(d < MOVES_PER_PLAYER) {
						if(l > alpha) {
							moves[d] = hashTable.move(idx);
							assert(moves[d] != Game::WRONG_MOVE);
							if(d < MOVES_PER_PLAYER - 1) {
								game.makeMove(moves[d]);
								alphabeta(d + 1, l - EPS, l + EPS);
								game.undoMove();
							}
						}
					}
					return l;
				}
				else {
					alpha = max(alpha, l);
					beta = min(beta, u);
					assert(alpha <= beta);
				}
			}
		}
	}

	// value of current state not found...
	if(game.endOfGame()) {
		float val = game.eval();
		while(val > 0. && d < MOVES_PER_PLAYER) {
			moves[d] = Game::WRONG_MOVE;
			++d;
		}
		return val;
	}
	else if(d >= MOVES_PER_PLAYER * depth) {
		assert(!game.endOfGame());
		assert(d == MOVES_PER_PLAYER * depth);
		float val = game.eval();
		hashTable.save(game.stateHash(), d, val, val, Game::WRONG_MOVE);
		return val;
	}
	else {
		float best = -Game::WIN_SCORE;
		typename Game::Move bestMv = Game::WRONG_MOVE;
		vector<typename Game::Move> legalMoves;

		for(unsigned int i = 0; i < game.legalMoves().size(); ++i) {
			uint64_t stateHash;
			if(d % MOVES_PER_PLAYER == 0) {
				stateHash = game.stateHash();
				assert(history.find(stateHash) == history.end());
				history.insert(stateHash);
			}
			typename Game::Move mv = game.legalMoves()[i];
			float val;
			game.makeMove(mv);
			if((d % MOVES_PER_PLAYER) == (MOVES_PER_PLAYER - 1)) {
				val = -1. * alphabeta(d + 1, -1. * beta, -1. * max(alpha, best));
			}
			else {
				val = alphabeta(d + 1, max(alpha, best), beta);
			}
			game.undoMove();
			if(d % MOVES_PER_PLAYER == 0) {
				history.erase(stateHash);
			}
			if(val > best) {
				assert(mv != Game::WRONG_MOVE);
				if(val > alpha && d < MOVES_PER_PLAYER) {
					moves[d] = mv;
				}
				assert(mv == game.legalMoves()[i]);
				if(val >= beta) {
					if(d % 2 == 0) {
						hashTable.save(game.stateHash(), d, val, Game::WIN_SCORE, mv);
					}
					return val;
				}
				best = val;
				bestMv = mv;
			}
		}
		if(d % 2 == 0) {
			if(best > alpha) {
				hashTable.save(game.stateHash(), d, best, best, bestMv);
			}
			else {
				hashTable.save(game.stateHash(), d, -Game::WIN_SCORE, best, bestMv);
			}
		}
		return best;
	}
}

#endif
