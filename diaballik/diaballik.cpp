#include "diaballik.h"
#include <iostream>
#include <cstring>
#include <cmath>
#include <cassert>
#include <queue>
#include <utility>
#include <climits>
#include <algorithm>

using namespace std;

const int GUARD = -1;
const int EMPTY = -2;
const int NO_BLOCKER = 100000;

const int shift[] = { 0, -9, +1, +9, -1, -10, -8, +8, +10 };

const float EPS = 0.00001;

const int BALL = 2;

struct Triple {
	Piece piece;
	Position pos;
	int dist;
};

Diaballik::Params::Params(int argc, char *argv[]) :
	tempoWeight(0.5),
	movesToWinWeight(0.5),
	ballMobilityWeight(5.),
	piecesMobilityWeight(3.)
{
	if(argc > 1) this->tempoWeight = atof(argv[1]);
	if(argc > 2) this->movesToWinWeight = atof(argv[2]);
	if(argc > 3) this->ballMobilityWeight = atof(argv[3]);
	if(argc > 4) this->piecesMobilityWeight = atof(argv[4]);
}

static inline int dist1(Position p1, Position p2) {
	int x1 = p1 / 9, y1 = p1 % 9,
			x2 = p2 / 9, y2 = p2 % 9;
	return abs(x1 - x2) + abs(y1 - y2);
}

static inline uint64_t rand64() {
	return ((uint64_t) rand() << 33) | ((uint64_t) rand() << 2) | ((uint64_t) rand() >> 27);
}

Diaballik::Diaballik(Params p) : params(p) {
	we = 0;
	pieceChosen = false;
	actionsDone = 0;
	throwLeft = true;
	movesLeft = 2;
	critCounter = 0;

	// memorize between which fields players can throw the ball
	memset(areInLine, 0, SIZE * SIZE);
	for(Position pos = 0; pos < SIZE; ++pos) {
		Position pos1 = 9 * (pos / 9), pos2 = pos % 9, pos3 = pos, pos4 = pos;
		int d1 = pos2, d2 = pos / 9, d3 = 0, d4 = 0;

		for(;pos3 >= 9 && pos3 % 9 != 0; pos3 -= 10) ++d3;
		for(;pos4 >= 9 && pos4 % 9 != 8; pos4 -= 8) ++d4;

		for(int d = 0; d < 9; ++d) {
			areInLine[pos][pos1] = true;
			dist[pos][pos1] = abs(d1 - d);
			direction[pos][pos1] = 0 + ((d1 - d) < 0 ? 1 : 0);

			areInLine[pos][pos2] = true;
			dist[pos][pos2] = abs(d2 - d);
			direction[pos][pos2] = 2 + ((d2 - d) < 0 ? 1 : 0);
			pos1 += 1; pos2 += 9;
		}

		for(int d = 0; pos3 % 9 != 8 && pos3 < 72; ++d) {
			areInLine[pos][pos3] = true;
			dist[pos][pos3] = abs(d3 - d);
			direction[pos][pos3] = 4 + ((d3 - d) < 0 ? 1 : 0);
			pos3 += 10;
		}

		for(int d = 0; pos4 % 9 != 0 && pos4 < 72; ++d) {
			areInLine[pos][pos4] = true;
			dist[pos][pos4] = abs(d4 - d);
			direction[pos][pos4] = 6 + ((d4 - d) < 0 ? 1 : 0);
			pos4 += 8;
		}
	}

	for(Piece piece = 0; piece < PIECES; ++piece) {
		pieces[WHITE].push_back(piece);
	}
	for(Piece piece = PIECES; piece < 2*PIECES; ++piece) {
		pieces[BLACK].push_back(piece);
	}

	for(Position pos = 0; pos < SIZE; ++pos) {
		if(pos < 9 || pos % 9 == 0 || pos % 9 == 8 || pos >= 72)
			board[pos] = GUARD;
		else
			board[pos] = EMPTY;
	}

	// player 1 configuration
	ball[WHITE] = 3;
	for(unsigned int i = 0; i < PIECES; ++i) {
		position[i] = 10 + i;
		board[10 + i] = i;
	}

	// player 2 configuration
	ball[BLACK] = 10;
	for(unsigned int i = 0; i < PIECES; ++i) {
		position[PIECES + i] = 64 + i;
		board[64 + i] = PIECES + i;
	}

	contactPoints = 0;

	initHash();
}

void Diaballik::initHash() {
	for(Position pos = 0; pos < SIZE; ++pos) {
		for(int p = 0; p < 5; ++p) {
			hashTable[pos][p] = rand64(); 
		}
	}
	hashPlayer = rand64();
	hashThrow[0] = rand64(); hashThrow[1] = rand64();
	hashMoves[0] = rand64(); hashMoves[1] = rand64(); hashMoves[2] = rand64();
	hashActions[0] = rand64(); hashActions[1] = rand64();
	hashActions[2] = rand64(); hashActions[3] = rand64();
	hashPieceChosen = rand64();
	hashValue = rand64();

	for(Position pos = 0; pos < SIZE; ++pos) {
		if(board[pos] == GUARD);
		else if(board[pos] == EMPTY)
			hashValue ^= hashTable[pos][4];
		else {
			int idx = WHITE;
			if(owns(BLACK, board[pos])) idx = BLACK;
			if(ball[WHITE] == board[pos] || ball[BLACK] == board[pos]) idx += BALL;
			hashValue ^= hashTable[pos][idx];
		}
	}
	hashValue ^= hashThrow[throwLeft ? 1 : 0];
	hashValue ^= hashMoves[movesLeft];
	hashValue ^= hashActions[actionsDone];
}


/*const std::vector<Diaballik::MoveT>& Diaballik::legalMoves() const {
	return moves;
}*/

Diaballik::MoveListT * Diaballik::getMoveList() {
	MoveListT * res = new MoveListT;

	if(pieceChosen) {
		if(ball[we] == currentPiece) {
			if(throwLeft) {
				for(int throwDir = 0; throwDir < THROW_DIRS; ++throwDir)
					closestEnemy[throwDir] = NO_BLOCKER;

				for(Piece enemyPiece = enemy(we) * PIECES; enemyPiece < (enemy(we) + 1) * PIECES; ++enemyPiece) {
					if(areInLine[position[currentPiece]][position[enemyPiece]]) {
						int line = direction[position[currentPiece]][position[enemyPiece]];
						closestEnemy[line] = min(closestEnemy[line], dist[position[currentPiece]][position[enemyPiece]]);
						assert(closestEnemy[line] > 0);
					}
				}

				for(Piece friendlyPiece = we * PIECES; friendlyPiece < (we + 1) * PIECES; ++friendlyPiece) {
					if((friendlyPiece != currentPiece || critCounter < 2) &&
							areInLine[position[currentPiece]][position[friendlyPiece]] &&
							(dist[position[currentPiece]][position[friendlyPiece]] <
							closestEnemy[direction[position[currentPiece]][position[friendlyPiece]]])) {
						assert(areInLine[position[currentPiece]][position[friendlyPiece]]);
						assert(areInLine[position[friendlyPiece]][position[currentPiece]]);
						res->list.push_back(position[friendlyPiece]);
					}
				}
			}
			else {
				res->list.push_back(position[currentPiece]);
			}
		}
		else {
			for(int dir = N; dir <= W; ++dir) {
				Position pos = position[currentPiece] + shift[dir];
				if(board[pos] == EMPTY)
					res->list.push_back(pos);
			}
		}
	}
	else if(movesLeft > 0) {
		for(unsigned int i = 0; i < PIECES; ++i) {
			res->list.push_back(position[we * PIECES + i]);
		}
	}
	else {
		res->list.push_back(position[ball[we]]);
	}

	res->position = 0;
	return res;
}

void Diaballik::rmMoveList(Diaballik::MoveListT * moveList) {
	delete moveList;
}

Diaballik::MoveT * Diaballik::getFirstMove(MoveListT * moves) {
	moves->position = 0;
	return &moves->list[moves->position];
}

Diaballik::MoveT * Diaballik::getNextMove(MoveListT * moves) {
	if(++moves->position >= moves->list.size())
		return NULL;
	else
		return &moves->list[moves->position];
}

void Diaballik::execute(Diaballik::MoveT move) {
	assert(pieceChosen);
	assert(move >= 0 && move < SIZE);
	assert(board[move] != GUARD);

	if(ball[we] == currentPiece) {
		assert(areInLine[position[currentPiece]][move]);
		assert(owns(we, board[move]));
		assert(owns(we, currentPiece));

		ball[we] = board[move];
		hashValue ^= hashTable[position[currentPiece]][we + BALL] ^
			hashTable[position[currentPiece]][we] ^
			hashTable[move][we] ^ hashTable[move][we + BALL];
	}
	else {
		assert(board[move] == EMPTY);
		assert(position[currentPiece] + shift[E] == move ||
				position[currentPiece] + shift[W] == move ||
				position[currentPiece] + shift[S] == move ||
				position[currentPiece] + shift[N] == move);

		if(contactsEnemy(currentPiece)) --contactPoints;

		hashValue ^= hashTable[position[currentPiece]][we] ^
			hashTable[position[currentPiece]][4] ^
			hashTable[move][4] ^ hashTable[move][we];

		board[position[currentPiece]] = EMPTY;
		board[move] = currentPiece;
		position[currentPiece] = move;

		if(contactsEnemy(currentPiece)) ++contactPoints;
	}
}

/*const Diaballik::MoveT * Diaballik::getPass() {
	assert(false);
	return NULL;
}*/

void Diaballik::makePass() {
	assert(false);
}

void Diaballik::undoPass() {
	assert(false);
}

bool Diaballik::canMove() {
	return true; // FIXME
}

void Diaballik::makeMove(Diaballik::MoveT const * move) {
	makeMove(*move);
}

void Diaballik::makeMove(Diaballik::MoveT const move) {
	Undo undo;
	hashValue ^= hashPieceChosen;

	if(pieceChosen) {
		hashValue ^= hashActions[actionsDone] ^ hashActions[actionsDone + 1];
		++actionsDone;
		if(ball[we] == currentPiece) {
			undo.piece = board[move];
			assert(owns(we, board[move]));
			assert(areInLine[move][position[currentPiece]] || (move == position[currentPiece]));
			if(board[move] != currentPiece) {
				hashValue ^= hashThrow[throwLeft ? 1 : 0] ^ hashThrow[0];
				throwLeft = false;
			}
			else
			{
				assert(critCounter < 2);
				++critCounter;
			}
		}
		else {
			undo.piece = currentPiece;
			hashValue ^= hashMoves[movesLeft] ^ hashMoves[movesLeft - 1];
			--movesLeft;
			assert(movesLeft >= 0);
		}
		assert(owns(we, currentPiece));
		undo.dest = position[currentPiece];
		assert(board[undo.dest] != GUARD);

		execute(move);
		pieceChosen = false;
	}
	else {
		// remember position to recognize draw
		//if(actionsDone == 0)
		//	forbiddenStates.insert(stateHash());

		assert(owns(we, board[move]));
		currentPiece = board[move];
		undo.piece = currentPiece;
		pieceChosen = true;
	}

	undo.movesLeft = movesLeft;
	undo.throwLeft = throwLeft;
	undo.critCounter = critCounter;
	history.push_back(undo);

	if(actionsDone >= 3) {
		assert(actionsDone == 3);
		hashValue ^= hashActions[actionsDone] ^ hashActions[0] ^
			hashThrow[throwLeft ? 1 : 0] ^ hashThrow[1] ^
			hashMoves[movesLeft] ^ hashMoves[2] ^
			hashPlayer;
		actionsDone = 0;
		throwLeft = true;
		movesLeft = 2;
		we = enemy(we);
		critCounter = 0;
	}
}

void Diaballik::undoMove() {
	hashValue ^= hashPieceChosen;
	assert(history.size() > 0);
	if(pieceChosen) {
		pieceChosen = false;
		history.pop_back();
		/*if(actionsDone == 0)
		{
			assert(forbiddenStates.find(stateHash()) != forbiddenStates.end());
			forbiddenStates.erase(stateHash());
		}*/
	}
	else {
		Undo undo = history.back();
		history.pop_back();
		assert(history.size() > 0);

		if(actionsDone == 0) {
			hashValue ^= hashActions[actionsDone] ^ hashActions[3] ^
				hashThrow[throwLeft ? 1 : 0] ^ hashThrow[undo.throwLeft ? 1 : 0] ^
				hashMoves[movesLeft] ^ hashMoves[undo.movesLeft] ^
				hashPlayer;
			actionsDone = 3;
			critCounter = undo.critCounter;
			throwLeft = undo.throwLeft;
			movesLeft = undo.movesLeft;
			we = enemy(we);
			assert(movesLeft >= 0);
		}

		currentPiece = undo.piece;
		pieceChosen = true;

		if(ball[we] == currentPiece) {
			if(board[undo.dest] != currentPiece) {
				hashValue ^= hashThrow[throwLeft ? 1 : 0] ^ hashThrow[1];
				throwLeft = true;
			}
			else
			{
				assert(critCounter > 0);
				--critCounter;
			}
		}
		else {
			hashValue ^= hashMoves[movesLeft] ^ hashMoves[movesLeft + 1];
			++movesLeft;
			assert(movesLeft < 3);
		}
		hashValue ^= hashActions[actionsDone] ^ hashActions[actionsDone - 1];
		--actionsDone;

		execute(undo.dest);

		currentPiece = (history.back()).piece;
	}
}

bool Diaballik::endOfGame() const {
	// we can recognize repeated positions only at the beginning of the move :(
/*	if(!pieceChosen && actionsDone == 0 &&
			forbiddenStates.find(stateHash()) != forbiddenStates.end())
	{
		return true;
	}*/

	if(contactPoints >= 3 &&
			(hasLine(WHITE) || hasLine(BLACK)))
		return true;

	return position[ball[WHITE]] >= 63 || position[ball[BLACK]] < 18;
}

Piece Diaballik::nextPiece(Piece piece) const {
	int player = piece / 7;

	if(owns(player, board[position[piece] + shift[E]]))
		return board[position[piece] + shift[E]];
	if(owns(player, board[position[piece] + shift[E] + shift[N]]))
		return board[position[piece] + shift[E] + shift[N]];
	if(owns(player, board[position[piece] + shift[E] + shift[S]]))
		return board[position[piece] + shift[E] + shift[S]];
	return GUARD;
}

bool Diaballik::hasLine(int player) const {
	for(Piece p = player * PIECES; p < (player + 1) * PIECES; ++p) {
		if(position[p] % 9 == 1) {
			for(Piece i = 0; i < PIECES - 1; ++i) {
				if((p = nextPiece(p)) == GUARD)
					return false;
			}
			return true;
		}
	}
	return false;
}

bool Diaballik::contactsEnemy(Piece piece) const {
	Direction dir = we == WHITE ? S : N;
	return owns(enemy(we), board[position[piece] + shift[dir]]);
}

string Diaballik::moveToStr(const Diaballik::MoveT move) {
	string res = "xx";
	assert(move > 0 && move < 81);
	res[0] = 'a' + move % 9 - 1;
	res[1] = '0' + move / 9;
	return res;
}

string Diaballik::printBoard() const {
	string res = "";
	for(int pos = 0; pos < SIZE; ++pos) {
		if(pos > 0 && pos < 8) {
			res += 'A' + pos - 1;
			res += " ";
		}
		else if(pos > 0 && pos < 72 && pos % 9 == 0) {
			res += '0' + pos / 9;
			res += " ";
		}
		else if(board[pos] == GUARD) res += "  ";
		else if(board[pos] == EMPTY) res += ". ";
		else if(board[pos] < PIECES) {
			if(ball[0] == board[pos])
				res += "W ";
			else
				res += "w ";
		}
		else {
			if(ball[1] == board[pos])
				res += "B ";
			else
				res += "b ";
		}

		if(pos % 9 == 8) res += "\n";
	}
	return res;
}

void Diaballik::makeMove(string move) {
	assert(!pieceChosen);
	int x1 = move[1] - '0', y1 = move[0] - 'a',
			x2 = move[3] - '0', y2 = move[2] - 'a';

	int pos1 = 9 * x1 + y1 + 1, pos2 = 9 * x2 + y2 + 1;

	assert(owns(we, board[pos1]));
	makeMove(pos1);//piece choice
	makeMove(pos2);
}

void Diaballik::forceEndTurn() {
	assert(!pieceChosen);
	for(int player = we; player == we; makeMove(position[ball[we]]));
}

float Diaballik::movesToWin(int player) {
	if(hasLine(enemy(player))) {
		int val = movesToContact(player);
		if(val <= 2) return (val + 1) / 2;
		else if(val <= 4) return (val + 1) / 3;
		else return (val + 1) / 4;
	}
	else {
		return movesToReachEnd(player);
	}
}

int Diaballik::movesToContact(int player) {	
	bool used[2 * PIECES];
	int res = 0;
	int step;

	memset(used, false, 2*PIECES);

	if(player == 0) step = shift[N];
	else step = shift[S];

	for(int i = 0; i < 3; ++i) {
		int closest = INT_MAX;
		int ours = -1, theirs = -1;
		for(Piece p = player * PIECES; p < (player + 1) * PIECES; ++p) {
			if(used[p]) continue;

			for(Piece e = enemy(player) * PIECES; e < (enemy(player) + 1) * PIECES; ++e) {
				if(used[e]) continue;
				int x1 = position[p] / 9, x2 = position[e] / 9;
				if(player == 0 && x1 >= x2) continue;
				if(player == 1 && x1 <= x2) continue;
				int contactPoint = position[e] + step;
				int d = dist1(position[p], contactPoint);
				if(d == INT_MAX) assert(false);
				if(d < closest) {
					closest = d;
					ours = p;
					theirs = e;
				}
			}
		}

		assert(ours != -1 && theirs != -1);
		res += closest;
		used[ours] = true;
		used[theirs] = true;
	}
	return res;
}

int Diaballik::movesToReachEnd(int player) {
	int M = 100;
	Position start;
	if(player == 0) start = 63;
	else start = 9;
	for(Position pos = start; pos < start + 9; ++pos) {
		if(bfsBallHelper[pos] < M) M = bfsBallHelper[pos];
	}

	return M;
}

inline Triple newTriple(Piece piece, Position pos, int dist) {
	Triple res;
	res.piece = piece;
	res.pos = pos;
	res.dist = dist;
	return res;
}

void Diaballik::piecesBfs(int player) {
	int boardEnd = (player == WHITE) ? 8 : 0;

	for(int i = 0; i < SIZE; ++i) {
		bfsPiece1Helper[i] = INT_MAX;
		bfsPiece2Helper[i] = INT_MAX;
		bfsClosest1Piece[i] = INT_MAX;
		bfsClosest2Piece[i] = INT_MAX;
	}

	queue<Triple> neighbours;

	for(Piece i = player * PIECES; i < (player + 1) * PIECES; ++i) {
		neighbours.push(newTriple(i, position[i], 0));
		bfsPiece1Helper[position[i]] = 0;
		bfsClosest1Piece[position[i]] = i;
		activity[i] = 0;
	}

	while(!neighbours.empty()) {
		Position pos = neighbours.front().pos;
		Piece piece = neighbours.front().piece;
		int dst = neighbours.front().dist;
		neighbours.pop();
		assert(bfsPiece2Helper[pos] >= bfsPiece1Helper[pos]);
		for(Direction dir = N; dir <= W; ++dir) {
			Position next = pos + shift[dir];
			float posVal = (float) abs(boardEnd - next / 9);
			if(board[next] == EMPTY ||
				(board[next] >= player * PIECES && board[next] < (player + 1) * PIECES)) {
				assert(bfsPiece2Helper[next] >= bfsPiece1Helper[next]);
				if(dst + 1 < bfsPiece1Helper[next]) {
					assert(piece != bfsClosest2Piece[next]);
					assert(piece != bfsClosest1Piece[next]);
					activity[piece] += posVal;
					if(bfsClosest2Piece[next] != INT_MAX) {
						activity[bfsClosest2Piece[next]] -= posVal;
						assert(activity[bfsClosest2Piece[next]] >= 0);
					}
					bfsPiece2Helper[next] = bfsPiece1Helper[next];
					bfsClosest2Piece[next] = bfsClosest1Piece[next];
					bfsPiece1Helper[next] = dst + 1;
					bfsClosest1Piece[next] = piece;
					neighbours.push(newTriple(piece, next, dst + 1));
					assert(bfsPiece2Helper[next] >= bfsPiece1Helper[next]);
				}
				else if(dst + 1 < bfsPiece2Helper[next] && piece != bfsClosest1Piece[next]) {
					assert(piece != bfsClosest2Piece[next]);
					assert(piece != bfsClosest1Piece[next]);
					activity[piece] += posVal;
					if(bfsClosest2Piece[next] != INT_MAX) {
						activity[bfsClosest2Piece[next]] -= posVal;
						assert(activity[bfsClosest2Piece[next]] >= 0);
					}
					bfsPiece2Helper[next] = dst + 1;
					bfsClosest2Piece[next] = piece;
					neighbours.push(newTriple(piece, next, dst + 1));
				}
			}
		}
	}

	piecesMobility = 0.;
	for(Piece i = player * PIECES; i < (player + 1) * PIECES; ++i) {
		piecesMobility += sqrt((float) activity[i]);
	}
}

void Diaballik::ballBfs(int player) {
	int boardEnd = (player == WHITE) ? 8 : 0;

	for(int i = 0; i < SIZE; ++i)
		bfsBallHelper[i] = INT_MAX;

	ballMobility = 0;
	queue<Triple> neighbours;

	// aproximate ball distance to all "empty" fields
	neighbours.push(newTriple(ball[player], position[ball[player]], 0));
	bfsBallHelper[position[ball[player]]] = 0;
	while(!neighbours.empty()) {
		Triple triple = neighbours.front();
		Position pos = triple.pos;
		Piece piece = triple.piece;
		int dst = triple.dist;
		neighbours.pop();
		for(Direction dir = N; dir <= SE; ++dir) {
			for(Position next = pos + shift[dir];
					board[next] == EMPTY || owns(player, board[next]);
					next += shift[dir]) {
				int diff;
				float posVal = 1. / (float) abs(boardEnd - (next / 9));
				Piece receiver;
				if(bfsClosest1Piece[next] == piece) {
					diff = bfsPiece2Helper[next];
					receiver = bfsClosest2Piece[next];
				}
				else {
					diff = bfsPiece1Helper[next];
					receiver = bfsClosest1Piece[next];
				}
				if(diff == INT_MAX) continue;
				int newDst = dst + max(1, (diff + 1) / 2);
				if(newDst < bfsBallHelper[next]) {
					if(bfsBallHelper[next] != INT_MAX)
						ballMobility -= posVal / bfsBallHelper[next];
					ballMobility += posVal / newDst;

					assert(ballMobility > 0 && ballMobility < 42);

					bfsBallHelper[next] = newDst;
					neighbours.push(newTriple(receiver, next, newDst));
				}
			}
		}
	}
}

float Diaballik::eval() {
	//case 0: draw (can be recognized only at the beginning of the move)
/*	if(!pieceChosen && actionsDone == 0 &&
			forbiddenStates.find(stateHash()) != forbiddenStates.end())
	{
		return DRAW_SCORE;
	}*/

	// case 1: unfair line
	if(contactPoints >= 3) {
		if(actionsDone == 0) {
			if(hasLine(enemy(we))) return WIN_SCORE;
			if(hasLine(we)) return -WIN_SCORE;
		}
		else {
			if(hasLine(we)) return -WIN_SCORE;
			if(hasLine(enemy(we))) return WIN_SCORE;
		}
	}

	// case 2: ball delivery
	if(position[ball[WHITE]] >= 63) {
		if(we == WHITE) return WIN_SCORE;
		else return -WIN_SCORE;
	}
	if(position[ball[BLACK]] < 18) {
		if(we == BLACK) return WIN_SCORE;
		else return -WIN_SCORE;
	}

	assert(!endOfGame());

	piecesBfs(we);
	ballBfs(we);

	float v1 = movesToWin(we), ball1 = ballMobility, pieces1 = piecesMobility;

	piecesBfs(enemy(we));
	ballBfs(enemy(we));

	float v2 = movesToWin(enemy(we)), ball2 = ballMobility, pieces2 = piecesMobility;

	float w1 = params.movesToWinWeight / min(v1, v2),
				w2 = (v1 + v2) * params.ballMobilityWeight,
				w3 = params.piecesMobilityWeight;

	assert(v1 > EPS && v2 > EPS);
	return ((v2 - v1 + params.tempoWeight) * 100. * w1 +
			(ball1 - ball2) * w2 +
			(pieces1 - pieces2) * w3) / (w1 + w2 + w3);
}
