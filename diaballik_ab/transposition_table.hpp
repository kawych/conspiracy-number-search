#ifndef __TRANSPOSITION_TABLE__
#define __TRANSPOSITION_TABLE__

#include <cstring>
#include <cstdlib>
#include <iostream>
#include <stdint.h>

using namespace std;

const unsigned int SEARCH = 4;
const uint32_t HASHTAB_SIZE = 1024 * 1024 * 32;
const uint64_t IDX_MASK = ~((uint64_t) 0) >> 39;

static inline int IDX(uint64_t hash) {
	return (hash & IDX_MASK);
}

class TranspositionTable {
	public:
		TranspositionTable();
		~TranspositionTable();
		int find(uint64_t h); // < 0 when not found
		float lower(int idx) const;
		float upper(int idx) const;
		int move(int idx) const;
		uint32_t depth(int idx) const;
		void save(uint64_t h, uint32_t d, float l, float u, int m);
		void erase(int idx);
		void clear();
	private:
		uint64_t* hash;
		uint32_t* depthT;
		float* lowerBd;
		float* upperBd;
		int* moveT;
};

TranspositionTable::TranspositionTable() {
	hash = new uint64_t[HASHTAB_SIZE];
	depthT = new uint32_t[HASHTAB_SIZE];
	lowerBd = new float[HASHTAB_SIZE];
	upperBd = new float[HASHTAB_SIZE];
	moveT = new int[HASHTAB_SIZE];
	memset(hash, 0, sizeof(uint64_t) * HASHTAB_SIZE);
}

TranspositionTable::~TranspositionTable() {
	delete[] hash;
	delete[] depthT;
	delete[] lowerBd;
	delete[] upperBd;
	delete[] moveT;
}

inline int TranspositionTable::find(uint64_t h){
	assert(h != 0);
	for(unsigned int i = 0; i < SEARCH; ++i) {
		if(hash[IDX(IDX(h) + i)] == h) {
			return IDX(IDX(h) + i);
		}
	}
	return -1;
}

inline float TranspositionTable::lower(int idx) const{
	assert(idx >= 0 && idx < HASHTAB_SIZE);
	return lowerBd[idx];
}

inline float TranspositionTable::upper(int idx) const{
	assert(idx >= 0 && idx < HASHTAB_SIZE);
	return upperBd[idx];
}

inline int TranspositionTable::move(int idx) const{
	assert(idx >= 0 && idx < HASHTAB_SIZE);
	return moveT[idx];
}

inline uint32_t TranspositionTable::depth(int idx) const{
	assert(idx >= 0 && idx < HASHTAB_SIZE);
	return depthT[idx];
}

void TranspositionTable::save(uint64_t h, uint32_t d, float l, float u, int m) {
	assert(l <= u);
	assert(h != 0);
	int idx = -1;
	uint32_t best = 0;
	for(unsigned int i = 0; i < SEARCH; ++i) {
		if(hash[IDX(IDX(h) + i)] == 0 || depthT[IDX(IDX(h) + i)] > best) {
			idx = IDX(IDX(h) + i);
			best = depthT[idx];
		}
	}
	assert(idx != -1);
	hash[idx] = h;
	depthT[idx] = d;
	lowerBd[idx] = l;
	upperBd[idx] = u;
	moveT[idx] = m;
}

inline void TranspositionTable::erase(int idx) {
	hash[idx] = 0;
}

inline void TranspositionTable::clear() {
	memset(hash, 0, sizeof(uint64_t) * HASHTAB_SIZE);
}

#endif
