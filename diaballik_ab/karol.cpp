#include <iostream>
#include <string>
#include <cassert>
#include "diaballik.h"
#include "alphabeta.hpp"

using namespace std;

const int BOUND1 = 10000; //10 sec
const int BOUND2 = 180000; //3 min

int main() {
	Diaballik game;
	AlphaBeta<Diaballik> alphabeta(game);
	int time;

	while(true) {
		string command;
		cin >> command;
		if(command == "time_left") {
			cin >> time;
			cout << "=" << endl;
			fflush(stdout);
		}
		else if(command == "play") {
			alphabeta.checkpoint();
			string move, player;
			cin >> player >> move;
			if(move.length() >= 4) {
				game.makeMove(move.substr(0, 4));
			}
			if(move.length() >= 8) {
				game.makeMove(move.substr(4, 4));
			}
			if(move.length() >= 12) {
				game.makeMove(move.substr(8, 4));
			}
			if(move.length() < 12) {
				game.forceEndTurn();
			}
			cout << "=" << endl;
			fflush(stdout);
		}
		else if(command == "gen_move") {
			string player;
			cin >> player;
			Diaballik::Move* moves;
			if(time < BOUND1) moves = alphabeta.bestMove(1);
			else if(time < BOUND2) moves = alphabeta.bestMove(2);
			else moves = alphabeta.bestMove(3);
			cout << "= ";
			for(int i = 0; i < 6; i += 2) {
				if(moves[i] != moves[i+1] && moves[i] != -1) {
					cout << game.moveToStr(moves[i]) << game.moveToStr(moves[i+1]);
				}
			}
			cout << endl;
			fflush(stdout);
		}
	}

	return 0;
}
