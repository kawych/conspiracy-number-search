#ifndef __DIABALLIK_H__
#define __DIABALLIK_H__

#include <vector>
#include <string>
#include <stdint.h>

typedef int Position;
typedef int Direction;
typedef int Piece;

enum { STAY = 0, N = 1, E = 2, S = 3, W = 4, NW = 5, NE = 6, SW = 7, SE = 8 };

const int MOVE_DIRS = 4;
const int THROW_DIRS = 8;
const int MOVES = 2;
const int THROWS = 1;
const int PIECES = 7;
const int SIZE = 81;
const int WHITE = 0, BLACK = 1;

class Diaballik {
	public:
		typedef int Move;
		static const float WIN_SCORE = 1000.;
		static const float DRAW_SCORE = 0.;
		static const Move WRONG_MOVE = -1;

		Diaballik();
		void makeMove(const Move move);
		void makeMove(std::string move);
		void undoMove();
		void forceEndTurn();
		const std::vector<Move>& legalMoves() const;
		bool endOfGame() const;
		float eval();
		int currentPlayer() const;
		uint64_t stateHash() const;

		std::string moveToStr(const Move move);
		std::string printBoard() const;
	private:
		struct Undo {
			Piece piece;
			Position dest;
			bool throwLeft;
			int movesLeft;
		};

		std::vector<Move> pieces[2];
		std::vector<Move> moves;
		std::vector<Undo> history;

		bool areInLine[SIZE][SIZE];
		int dist[SIZE][SIZE];
		int direction[SIZE][SIZE];
		int closestEnemy[THROW_DIRS];
		int board[SIZE];
		int we;
		int position[2 * PIECES];
		int ball[2];
		int contactPoints;
		Piece currentPiece;
		bool pieceChosen;
		bool throwLeft;
		int movesLeft;
		int actionsDone;

		int bfsBallHelper[SIZE];
		int bfsPiece1Helper[SIZE];
		int bfsPiece2Helper[SIZE];
		int bfsClosest1Piece[SIZE];
		int bfsClosest2Piece[SIZE];
		float activity[2 * PIECES];
		float ballMobility;
		float piecesMobility;

		uint64_t hashTable[SIZE][5];
		uint64_t hashPlayer;
		uint64_t hashThrow[2];
		uint64_t hashMoves[3];
		uint64_t hashActions[4];
		uint64_t hashValue;

		void initHash();

		int enemy(int player) const;
		void generateMoves();
		void execute(Move move);
		bool contactsEnemy(Piece piece) const;
		Piece nextPiece(Piece p) const;
		bool hasLine(int player) const;
		bool owns(int player, Piece piece) const;

		void piecesBfs(int player);
		void ballBfs(int player);
		int movesToContact(int player);
		int movesToReachEnd(int player);
		float movesToWin(int player);
};

inline int Diaballik::currentPlayer() const {
	return we;
}

inline int Diaballik::enemy(int player) const {
	return 1 - player;
}

inline bool Diaballik::owns(int player, Piece piece) const {
	return piece >= player * PIECES && piece < (player + 1) * PIECES;
}

inline uint64_t Diaballik::stateHash() const {
	return hashValue;
}

#endif
