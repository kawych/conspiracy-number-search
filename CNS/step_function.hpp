#ifndef __STEP_FUNCTION__
#define __STEP_FUNCTION__


#include <cstdlib>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <stdint.h>
#include <set>
#include <vector>
#include <climits>
#include <cfloat>
#include <cstring>
#include <cassert>
#include "../include/debug_things.h"
//#include "transposition_table.hpp"

using namespace std;

#ifdef const_d
const float D = const_d;
#else
const float D = 1.25;
#endif

template <class Game>
class Stair
{
	public:
		typename Game::EvalT arg;
		unsigned int val;
		Stair(typename Game::EvalT a, unsigned int v) :
			arg(a),
			val(v)
		{ }
};

template <class Game>
class StepFun
{
	public:
		void init(typename Game::EvalT);
		void init(typename Game::EvalT, unsigned int);
		void funcSum(StepFun<Game> ** functions, int n);
		void funcMin(StepFun<Game> ** functions, int n);
		inline typename Game::EvalT nthArg(int i);
		inline unsigned int nthVal(int i);
		inline size_t size();
		inline void clear();
		void print();
		typename Game::EvalT argFor(unsigned int val);
		unsigned int valAt(typename Game::EvalT arg);
		typename Game::EvalT lastZeroArg();
		unsigned int firstNonzeroVal();
		bool isMonotonious();

		inline void addStair(typename Game::EvalT arg, unsigned int val);
	private:
		static const typename Game::EvalT minArg = Game::MIN_SCORE;
		static const typename Game::EvalT maxArg = Game::MAX_SCORE;
		vector<Stair<Game> > function;

		void repairHeap(StepFun<Game> ** heap, unsigned int * ptr,
				int from, int to);
		void repairMaxHeap(StepFun<Game> ** heap, unsigned int * ptr,
				int from, int to);
};

template <class Game>
inline void StepFun<Game>::addStair(typename Game::EvalT arg, unsigned int val)
{
	function.push_back(Stair<Game>(arg, val));
}

template <class Game>
void StepFun<Game>::print()
{
	cerr << "[ ";
	for(unsigned int i = 0; i < function.size(); ++i)
	{
		cerr << function[i].arg << "->" << function[i].val << " ";
	}
	cerr << "]" << endl;
}

template<class Game>
inline void StepFun<Game>::init(typename Game::EvalT arg)
{
	clear();
#if LEAF == 1
	if(D == 1.0)
	{
#ifdef DIABALLIK
		for(int i = 1; i < 100; ++i)
#endif
#ifdef OTHELLO
		for(int i = 1; arg + i < maxArg; ++i)
#endif
		{
			// for othello
			// hard to do for diaballik...
			addStair(arg + ((typename Game::EvalT) (i - 1) * Game::CN_STEP),
						(unsigned int) i);
		}
	}
	else
	{
#ifdef DIABALLIK
		for(int i = 1; i < 100; i = ceil((float) i * D))
#endif
#ifdef OTHELLO
		for(int i = 1; arg + i - 1 < maxArg &&
				(arg + ((typename Game::EvalT) (i - 1) * Game::CN_STEP) < Game::MAX_SCORE);
				i = ceil((float) i * D))
#endif
		{
			addStair(arg + ((typename Game::EvalT) (i - 1) * Game::CN_STEP),
						(unsigned int) i);
		}
	}
#else // LEAF == 0
	if(arg < Game::MAX_SCORE)
		addStair(arg, 1);
#endif
	addStair(Game::MAX_SCORE, UINT_MAX / 2);
}

template<class Game>
inline void StepFun<Game>::init(typename Game::EvalT arg, unsigned int val)
{
	clear();
	addStair(arg, val);
}

template<class Game>
inline typename Game::EvalT StepFun<Game>::nthArg(int i)
{
	return function[i].arg;
}

template <class Game>
typename Game::EvalT StepFun<Game>::lastZeroArg()
{
	return function[0].arg;
}

template <class Game>
unsigned int StepFun<Game>::firstNonzeroVal()
{
	assert(size() > 0);
	return function[0].val;
}

template<class Game>
inline unsigned int StepFun<Game>::nthVal(int i)
{
	return function[i].val;
}

template<class Game>
inline size_t StepFun<Game>::size()
{
	return function.size();
}

template<class Game>
inline void StepFun<Game>::clear()
{
	function.clear();
}

template <class Game>
void StepFun<Game>::repairHeap(StepFun<Game> ** heap, unsigned int * ptr,
		int from, int n)
{
	int son;
	for(int i = from; i <= n / 2 - 1; i = son)
	{
		assert(i * 2 + 1 < n);
		son = 2 * i + 1;
		assert(heap[son] != NULL);
		typename Game::EvalT arg = heap[son]->nthArg(ptr[son]);
		if(son + 1 < n)
		{
			if(heap[son + 1]->nthArg(ptr[son + 1]) < arg)
			{
				++son;
				arg = heap[son]->nthArg(ptr[son]);
			}
		}

		if(arg < heap[i]->nthArg(ptr[i]))
		{
			StepFun * tmp1 = heap[i];
			unsigned int tmp2 = ptr[i];
			heap[i] = heap[son];
			ptr[i] = ptr[son];
			heap[son] = tmp1;
			ptr[son] = tmp2;
		}
		else
			break;
	}
}

template <class Game>
void StepFun<Game>::funcSum(StepFun<Game> ** heap, int n)
{
	clear();
	unsigned int * ptr = new unsigned int[n];
	memset(ptr, 0, n * sizeof(unsigned int));
	for(int i = 0; i < n; ++i)
		assert(ptr[i] == 0);

	// construct heap
	for(int i = n / 2 - 1; i >= 0; --i)
	{
		repairHeap(heap, ptr, i, n);
	}

	unsigned int nextVal = 0;
	unsigned int highVal = 0;
	float limit = -1.;
	int last;
	for(last = -1; n > 0;)
	{
		assert(last < 0 || heap[0]->nthArg(ptr[0]) >= nthArg(last));
		if(last >= 0 && heap[0]->nthArg(ptr[0]) <= nthArg(last))
		{
			//
			assert(heap[0]->nthArg(ptr[0]) == nthArg(last));
			assert(function[last].val <= UINT_MAX / 2);
			if(ptr[0] > 0)
			{
				if(!(function[last].val > heap[0]->nthVal(ptr[0] - 1)))
				{
					cerr << n << endl;
					cerr << last << endl;
					cerr << function[last].arg << " -> " <<  function[last].val << endl;
					cerr << heap[0]->nthVal(ptr[0] - 1) << endl;
					cerr << heap[0]->nthVal(ptr[0]) << endl;
					print();
				}
				assert(function[last].val > heap[0]->nthVal(ptr[0] - 1));
				//
				assert(heap[0]->nthVal(ptr[0] - 1) >= 0);
				assert(function[last].val <= UINT_MAX / 2);
				function[last].val -= heap[0]->nthVal(ptr[0] - 1);
			}
			if(function[last].val > UINT_MAX / 2)
			{
				cerr << function[last].val << " > " << UINT_MAX / 2 << endl;
				for(unsigned int b = 0; b < function.size(); ++b)
				{
					cerr << " " << function[b].arg << "->" << function[b].val << " ";
				}
				cerr << endl;
			}
			assert(function[last].val <= UINT_MAX / 2);

			// increase current stair
			function[last].val += heap[0]->nthVal(ptr[0]);

			// infinity + infinity = infinity
			if(function[last].val > UINT_MAX / 2)
				function[last].val = UINT_MAX / 2;

			nextVal = function[last].val;
			assert(nextVal == UINT_MAX / 2 || nextVal < UINT_MAX / 4);
		}
		else
		{
			if(nextVal < UINT_MAX / 2)
				nextVal += heap[0]->nthVal(ptr[0]);
			if(nextVal > UINT_MAX / 2)
			{
				nextVal = UINT_MAX / 2;
			}
			else if(ptr[0] > 0)
			{
				assert(nextVal <= UINT_MAX / 2);
				assert(heap[0]->nthVal(ptr[0]) > heap[0]->nthVal(ptr[0] - 1));
				assert(nextVal >= heap[0]->nthVal(ptr[0] - 1));
				if(nextVal < UINT_MAX / 2)
					nextVal -= heap[0]->nthVal(ptr[0] - 1);
				assert(nextVal <= UINT_MAX / 2);
			}

			// add new stair
			if(function.size() == 0)
			{
				addStair(heap[0]->nthArg(ptr[0]), nextVal);
				++last;
			}
			else if(function.size() == 1)
			{
				highVal = function[last].val;
				limit = D * (float) highVal;
				addStair(heap[0]->nthArg(ptr[0]), nextVal);
				++last;
			}
			else
			{
				if((float) function[last].val <= limit)
				{
					function[last - 1].val = function[last].val;
					function[last].arg = heap[0]->nthArg(ptr[0]);
					function[last].val = nextVal;
				}
				else
				{
					highVal = function[last].val;
					limit = D * (float) highVal;
					addStair(heap[0]->nthArg(ptr[0]), nextVal);
					++last;
				}
			}

		}

		++ptr[0];

		// repair heap
		if(heap[0]->size() == ptr[0])
		{
			heap[0] = heap[--n];
			ptr[0] = ptr[n];
		}
		repairHeap(heap, ptr, 0, n);
	}
	if(last > 0 && (float) function[last].val <= limit)
	{
		function[last - 1].val = function[last].val;
		function.pop_back();
	}


	delete [] ptr;
}

template <class Game>
void StepFun<Game>::funcMin(StepFun<Game> ** heap, int n)
{
	clear();
	unsigned int * ptr = new unsigned int[n];
	memset(ptr, 0, n * sizeof(unsigned int));
	int maxIdx = -1;

	//find the latest "stair"
	typename Game::EvalT maxArg = -Game::INF_SCORE;
	for(int i = 0; i < n; ++i)
	{
		assert(heap[i] != NULL);
		assert(ptr[i] == 0);
		assert(heap[i]->size() > 0);
		if(heap[i]->nthArg(ptr[i]) > maxArg)
		{
			maxArg = heap[i]->nthArg(ptr[i]);
			maxIdx = i;
		}
	}

	unsigned int highVal = 0;
	float limit = -1.;
	int last = -1;
	do
	{
		assert(maxIdx >= 0);
		assert(maxIdx < n);
		maxArg = heap[maxIdx]->nthArg(ptr[maxIdx]);

		//find the lowest value for current stair
		unsigned int minVal = UINT_MAX;
		for(int i = 0; i < n; ++i)
		{
			// skip all stairs before current one
			for(; ptr[i] + 1 < heap[i]->size() &&
					heap[i]->nthArg(ptr[i] + 1) <= maxArg; ++ptr[i]);

			// find minimal value for fixed argument maxArg
			if(heap[i]->nthVal(ptr[i]) < minVal)
			{
				minVal = heap[i]->nthVal(ptr[i]);
				maxIdx = i;
			}
		}
		assert(minVal != UINT_MAX);

		// avoid multiple stairs with the same value
		if(function.size() == 0 || (float) minVal > limit)
		{
			if(function.size() == 0)
			{
				assert(last == -1);
				highVal = minVal;
			}
			else
			{
				assert(highVal != 0 && limit != -1.);
				function[last].val = highVal;
			}
			assert(function.size() == 0 || function.back().val < minVal);
			addStair(maxArg, minVal);
			++last;
			highVal = minVal;
			limit = D * (float) highVal;
		}
		else
		{
			highVal = minVal;
		}

		++ptr[maxIdx];
	}
	while(ptr[maxIdx] < heap[maxIdx]->size());

	delete [] ptr;
}

// maximal argument arg such that f(arg) <= val
template <class Game>
typename Game::EvalT StepFun<Game>::argFor(unsigned int val)
{
	assert(val < UINT_MAX / 2);
	if(function[0].val > val && function[0].val < UINT_MAX / 2)
		return function[0].arg + Game::EPSILON;
	for(unsigned int i = 0; i < size(); ++i)
	{
		if(function[i].val > val)
			return function[i].arg;
	}

	return Game::INF_SCORE;
}

template <class Game>
unsigned int StepFun<Game>::valAt(typename Game::EvalT arg)
{
	if(arg <= function[0].arg)
		return 0;
	for(unsigned int i = 0; i < size() - 1; ++i)
		if(arg <= function[i + 1].arg)
			return function[i].val;
	return function.back().val;
}

template <class Game>
bool StepFun<Game>::isMonotonious()
{
	for(unsigned int i = 0; i < size() - 1; ++i)
	{
		assert(function[i].arg < function[i + 1].arg);
		if(function[i].val >= function[i + 1].val)
			return false;
	}
	return true;
}


#endif
