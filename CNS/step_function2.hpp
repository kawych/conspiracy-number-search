#ifndef __STEP_FUNCTION__
#define __STEP_FUNCTION__


#include <cstdlib>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <stdint.h>
#include <set>
#include <vector>
#include <climits>
#include <cfloat>
#include <cstring>
#include <cassert>
#include "../include/debug_things.h"
//#include "transposition_table.hpp"

using namespace std;

template <class Game>
class StepFun
{
	public:
		void init(typename Game::EvalT);
		void init(typename Game::EvalT, unsigned int);

		void funcSum(StepFun<Game> ** functions, int n);
		void funcMin(StepFun<Game> ** functions, int n);
		void print();
		typename Game::EvalT argFor(unsigned int val);
		unsigned int valAt(typename Game::EvalT arg);
		typename Game::EvalT lastZeroArg();
		unsigned int firstNonzeroVal();
		bool isMonotonious();

	private:
		static const typename Game::EvalT minArg = Game::MIN_SCORE;
		static const typename Game::EvalT maxArg = Game::MAX_SCORE;
		unsigned int function[maxArg - minArg + 1];
};

template <class Game>
void StepFun<Game>::init(typename Game::EvalT arg)
{
	typename Game::EvalT i;
	for(i = minArg; i <= arg; ++i)
		function[i - minArg] = 0;

#if LEAF == 1
	for(; i <= maxArg; ++i)
		function[i - minArg] = i - arg;
#else
	for(; i <= maxArg; ++i)
		function[i - minArg] = 1;
#endif
}

template <class Game>
void StepFun<Game>::init(typename Game::EvalT arg, unsigned int val)
{
	typename Game::EvalT i;
	for(i = minArg; i <= arg; ++i)
		function[i - minArg] = 0;

	for(; i <= maxArg; ++i)
		function[i - minArg] = val;
}

template <class Game>
void StepFun<Game>::print()
{
	cerr << "[ ";
	for(typename Game::EvalT i = minArg; i <= maxArg; ++i)
		cerr << i << "->" << function[i - minArg] << " ";
	cerr << " ]" << endl;
}

template <class Game>
void StepFun<Game>::funcSum(StepFun<Game> ** funs, int n)
{
	assert(n > 0);
	typename Game::EvalT i;
	for(i = minArg; i <= maxArg; ++i)
		function[i - minArg] = 0;

	for(int f = 0; f < n; ++f)
		for(i = minArg; i <= maxArg; ++i)
			if(function[i - minArg] < UINT_MAX / 2)
			{
				assert(funs[f]->function[i - minArg] <= UINT_MAX / 2);
				function[i - minArg] += funs[f]->function[i - minArg];
			}

	for(i = minArg; i <= maxArg; ++i)
		if(function[i - minArg] >= UINT_MAX / 2)
			function[i - minArg] = UINT_MAX / 2;
}

template <class Game>
void StepFun<Game>::funcMin(StepFun<Game> ** funs, int n)
{
	assert(n > 0);
	typename Game::EvalT i;
	for(i = minArg; i <= maxArg; ++i)
		function[i - minArg] = UINT_MAX;

	for(int f = 0; f < n; ++f)
		for(i = minArg; i <= maxArg; ++i)
			if(function[i - minArg] > funs[f]->function[i - minArg])
				function[i - minArg] = funs[f]->function[i - minArg];
}

// minimal argument arg such that f(arg) >= val
template <class Game>
typename Game::EvalT StepFun<Game>::argFor(unsigned int val)
{
	for(typename Game::EvalT i = minArg; i < maxArg; ++i)
		if(function[i - minArg] >= val)
		{
			if(function[i - minArg] >= UINT_MAX / 2)
			{
				assert(i > minArg);
				return i - 1;
			}
			else
				return i;
		}

	return Game::MAX_SCORE;
}

template <class Game>
unsigned int StepFun<Game>::valAt(typename Game::EvalT arg)
{
	if(arg < minArg)
	{
		assert(false);
		return function[0];
	}
	else if(arg > maxArg)
	{
		return UINT_MAX / 2;
	}
//		return function[maxArg - minArg];
	else
		return function[arg - minArg];
}

template <class Game>
typename Game::EvalT StepFun<Game>::lastZeroArg()
{
	// linear search for now
	for(typename Game::EvalT i = minArg; i <= maxArg; ++i)
		if(function[i - minArg] > 0)
		{
			assert(i > minArg);
			return i - 1;
		}

	return Game::MAX_SCORE;
}

template <class Game>
unsigned int StepFun<Game>::firstNonzeroVal()
{
	return valAt(lastZeroArg() + 1);
}

template <class Game>
bool StepFun<Game>::isMonotonious()
{
	for(typename Game::EvalT i = minArg; i < maxArg; ++i)
		if(function[i - minArg] > function[i - minArg + 1])
			return false;

	return true;
}

#endif
