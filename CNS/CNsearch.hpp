#ifndef __CN_SEARCH_H__
#define __CN_SEARCH_H__

#include <cstdlib>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <stdint.h>
#include <set>
#include <vector>
#include <climits>
#include <cfloat>
#include <cstring>
#include <cassert>

#ifdef DIABALLIK
	#include "step_function.hpp"
#endif

#ifdef OTHELLO
	#if STEP_FUN == 1
		#include "step_function.hpp"
	#else
		#include "step_function2.hpp"
	#endif
#endif

#include "../include/debug_things.h"
#include "transposition_table.hpp"

using namespace std;

const int PROOF_FUNC = 0;
const int DISPROOF_FUNC = 1;

const unsigned int CN_INF = UINT_MAX / 2;

#ifdef ct
const unsigned int C = ct;
#else
const unsigned int C = 5;//default
#endif

#ifdef ab_lv
const unsigned int SMOOTH_LEVEL = ab_lv;
#else
const unsigned int SMOOTH_LEVEL = 2;//default
#endif

#if use_tt == false
const bool USE_TT = false;
#else
const bool USE_TT = true; // default
#endif

#ifdef DIABALLIK
const bool PASS_ALLOWED = false;
const bool STATES_REPEAT = true;
#endif

#ifdef OTHELLO
const bool PASS_ALLOWED = true;
const bool STATES_REPEAT = false;
#endif

#ifdef max_dfs_iter
const int MAX_DFS_ITER = max_dfs_iter;
#else
const int MAX_DFS_ITER = 50;
#endif

template <class Game>
union cn_func
{
	typename Game::EvalT arg;
	unsigned int val;

	cn_func(typename Game::EvalT x) : arg(x) { }
	cn_func(unsigned int x) : val(x) { }
	cn_func(const cn_func & x) : val(x.val) { }
	void operator=(typename Game::EvalT arg) {
		this->arg = arg;
	}
	void operator=(unsigned int val) {
		this->val = val;
	}
	void operator=(const cn_func & x) {
		this->val = x.val;
	}
};

template <class Game>
class CNSearch {
	public:
		typedef typename Game::EvalT EvalT;
		typedef typename Game::MoveT MoveT;
		typedef typename Game::MoveListT MoveListT;
		typedef typename Game::HashT HashT;

		// conspiracy numbers
		struct ConNums {
			uint32_t visits;
			StepFun<Game> f[2];
		};

		struct Tree {
			EvalT value;
			//MoveListT moveList;
			// CN.f[PROOF_FUNCTION] - proof function
			// PF(dv) = how many leafs need to be changed
			// in order to increase state value of <dv>
			ConNums CN;
			vector<Tree *> moves;

			// diagnostic info
			Tree * parent;
			EvalT initialValue;
			int nodesExpanded;
			bool hasRef;

			bool isLeaf();
			Tree * nthAnc(int n) {
				if(n == 1) return parent;
				else if(n > 1) return parent->nthAnc(n - 1);
				else return NULL;
			}
		};
		CNSearch(Game * game);
		CNSearch();

		typename Game::EvalT evalPosition(struct Tree * tree,
				unsigned int iterations, unsigned int movesMade);
		typename Game::EvalT evalPositionDFS(struct Tree * tree,
				unsigned int iterations, unsigned int movesMade);
		typename Game::MoveT * bestMove(unsigned int iterations);

		void checkpoint();
		struct Tree * newTree(EvalT eval);
		void cleanTree(struct Tree * tree);
		void developNode(struct Tree * tree, int depth);
		void updateNode(struct Tree * node, int depth);
		void setGame(Game * game);
	private:
		Game * game;
		int hits;
		int misses;
		int lostIterations;

		void updateSum(struct Tree * node, int function);
		void updateMin(struct Tree * node, int function);

		void search(struct Tree * node, EvalT currArg,
				EvalT sndArg, int currFunc, int depth);
		void searchLeaf(struct Tree * node, int depth);
		void searchInternalNode(struct Tree * node,
				EvalT currArg, EvalT sndArg, int currFunc, int depth);
		void chooseMove(struct Tree * node,  EvalT currArg, int currFunc,
				MoveListT * moveList, unsigned int &idx, MoveT * &chosenMove);

		int searchDFS(struct CNSearch<Game>::Tree * &node,
				EvalT currArg, EvalT sndArg, unsigned int currTH,
				unsigned int sndTH, int currFunc, int depth, int iterations);
		int searchDeeper(struct Tree * &node, EvalT currArg, EvalT sndArg,
				unsigned int currTH, unsigned int sndTH, int currFunc, int depth,
				int iterations);
		int passDeeper(struct Tree * &node, EvalT currArg,
				EvalT sndArg, unsigned int currTH, unsigned int sndTH, int currFunc,
				int depth, int iterations);

		void printLog(struct Tree * node, int depth);
		EvalT flatSearch(int smoothLevel, int depth);
		bool firstSmallMove(int depth);
		bool lastSmallMove(int depth);
		bool firstPlayer(int depth);
		bool blindAlley(struct Tree * node);

		multiset<uint64_t> history;
		typename Game::MoveT moves[Game::MOVES_PER_PLAYER];
		TranspositionTable<HashT, ConNums> hashTable;
};

template <class Game>
CNSearch<Game>::CNSearch(Game * g) : game(g) { }

template <class Game>
CNSearch<Game>::CNSearch() : game(NULL) { }

template <class Game>
void CNSearch<Game>::setGame(Game * g)
{
	game = g;
}

template <class Game>
inline void CNSearch<Game>::checkpoint() {
	history.insert(game->stateHash());
}

template <class Game>
struct CNSearch<Game>::Tree * CNSearch<Game>::newTree(EvalT eval)
{
	struct CNSearch<Game>::Tree * tree = new struct CNSearch<Game>::Tree();
	tree->value = eval;
	tree->CN.visits = 0;
	tree->nodesExpanded = 1;
	tree->initialValue = eval;
	tree->hasRef = false;

	if(game->endOfGame())
	{
		tree->CN.f[PROOF_FUNC].init(eval, UINT_MAX / 2);
		tree->CN.f[DISPROOF_FUNC].init(-eval, UINT_MAX / 2);
	}
	else if(STATES_REPEAT && history.find(game->stateHash()) != history.end())
	{
		tree->CN.f[PROOF_FUNC].init(Game::DRAW_SCORE, UINT_MAX / 2);
		tree->CN.f[DISPROOF_FUNC].init(-Game::DRAW_SCORE, UINT_MAX / 2);
		tree->value = Game::DRAW_SCORE;
	}
	else
	{
		tree->CN.f[PROOF_FUNC].init(eval);
		tree->CN.f[DISPROOF_FUNC].init(-eval);
	}

	return tree;
}

template <class Game>
void CNSearch<Game>::cleanTree(struct CNSearch<Game>::Tree * tree)
{
	for(unsigned int i = 0; i < tree->moves.size(); ++i)
	{
		if(!PASS_ALLOWED && tree->moves[i] == NULL)
		{
			continue;
		}

		assert(tree->moves[i] != NULL);
		cleanTree(tree->moves[i]);
	}

	delete tree;
}

template <class Game>
void CNSearch<Game>::developNode(struct CNSearch<Game>::Tree * tree, int depth)
{
	assert(!game->endOfGame());
	assert(tree->moves.size() == 0u);
	// Multiplier needed, because each player returns a game evaluation
	// from his point of view.
	EvalT multiplier;
	if(firstPlayer(depth + 1))
		multiplier = 1;
	else
		multiplier = -1;

	// probably we can remove this, the state after one move can not be the same
	if(STATES_REPEAT && firstSmallMove(depth))
	{
		history.insert(game->stateHash());
	}
	if(PASS_ALLOWED && !game->canMove())
	{
		game->makePass();
		if(STATES_REPEAT && history.find(game->stateHash()) != history.end())
		{
			assert(firstSmallMove(depth + 1));
			tree->moves.push_back(newTree(Game::DRAW_SCORE));
		}
		else if(USE_TT)
		{
			ConNums * entry = hashTable.find(game->stateHash());
			if(entry != NULL)
			{
				++hits;
				tree->moves.push_back(newTree(entry->f[PROOF_FUNC].lastZeroArg()));
				tree->moves.back()->CN = *entry;
			}
			else
			{
				++misses;
				tree->moves.push_back(newTree(multiplier * game->eval()));
			}
		}
		else
		{
			tree->moves.push_back(newTree(multiplier * game->eval()));
		}

		tree->moves.back()->parent = tree;//dbg

		game->undoPass();
	}
	else
	{
		MoveListT * moveList = game->getMoveList();
		for(MoveT * mv = game->getFirstMove(moveList); mv; mv = game->getNextMove(moveList))
		{
			game->makeMove(mv);
			if(STATES_REPEAT && history.find(game->stateHash()) != history.end())
			{
				assert(firstSmallMove(depth + 1));
				tree->moves.push_back(newTree(Game::DRAW_SCORE));
			}
			else if(USE_TT)
			{
				ConNums * entry = hashTable.find(game->stateHash());
				if(entry != NULL)
				{
					++hits;
					tree->moves.push_back(newTree(entry->f[PROOF_FUNC].lastZeroArg()));
					tree->moves.back()->CN = *entry;
					//tree->moves.back()->value = entry->f[PROOF_FUNC].lastZeroArg();
				}
				else
				{
					++misses;
					tree->moves.push_back(newTree(multiplier * game->eval()));
				}
			}
			else
			{
				tree->moves.push_back(newTree(multiplier * game->eval()));
			}

			tree->moves.back()->parent = tree;//dbg


			// with smoothing - better(?) but slow
			/*float val1, val2;
			val1 = multiplier * game->eval();
			val2 = multiplier * flatSearch(SMOOTH_LEVEL, depth + 1);
			tree->moves.push_back(newTree((val1 + val2) / 2.));*/

			game->undoMove();
		}
		game->rmMoveList(moveList);
	}
	if(STATES_REPEAT && firstSmallMove(depth))
	{
		assert(history.find(game->stateHash()) != history.end());
		history.erase(game->stateHash());
	}
}


// Simple min-max to level smoothLevel. Returns game value from current
// player's point of view.
template <class Game>
typename Game::EvalT CNSearch<Game>::flatSearch(int smoothLevel, int depth)
{
	if(smoothLevel == 0 || !game->canMove())
	{
		return game->eval();
	}
	else
	{
		EvalT eval = -Game::INF_SCORE;
		for(MoveT mv = game->getFirstMove(); mv; mv = game->getNextMove())
		{
			game->makeMove(mv);

			EvalT v = flatSearch(smoothLevel - 1, depth + 1);
			if(v > eval)
				eval = v;

			game->undoMove();
		}
		if(firstSmallMove(depth))
			return -eval;
		else
			return eval;
	}
}


template <class Game>
void CNSearch<Game>::updateSum(struct CNSearch<Game>::Tree * node, int function)
{
	if(debug)
	{
		assert(node->moves.size() > 0);

		// we also assume that at least one move is not NULL
		bool OK = false;
		for(unsigned int i = 0; i < node->moves.size(); ++i)
			if(node->moves[i] != NULL)
				OK = true;

		assert(OK);
		_unused(OK);
	}

	unsigned int n = node->moves.size();
	assert(n > 0);
	StepFun<Game> ** functions = new StepFun<Game>*[n];
	memset(functions, 0, n * sizeof(StepFun<Game> **));
	unsigned int j = 0;
	for(unsigned int i = 0; j < n; ++i)
	{
		assert(functions[j] == NULL);
		if(node->moves[i] != NULL)
		{
			functions[j] = &node->moves[i]->CN.f[function];
			++j;
		}
		else // skip blind alley
			--n;
	}

	node->CN.f[function].funcSum(functions, n);

	delete[] functions;
}

template <class Game>
void CNSearch<Game>::updateMin(struct CNSearch<Game>::Tree * node, int function)
{
	if(debug)
	{
		assert(node->moves.size() > 0);
		// we also assume that at least one move is not NULL
		bool OK = false;
		for(unsigned int i = 0; i < node->moves.size(); ++i)
			if(node->moves[i] != NULL)
				OK = true;
		assert(OK);
		_unused(OK);
	}

	unsigned int n = node->moves.size();
	assert(n > 0);
	StepFun<Game> ** functions = new StepFun<Game>*[n];
	memset(functions, 0, n * sizeof(StepFun<Game> **));
	unsigned int j = 0;
	for(unsigned int i = 0; j < n; ++i)
	{
		assert(functions[j] == NULL);
		if(node->moves[i] != NULL)
		{
			functions[j] = &node->moves[i]->CN.f[function];
			++j;
		}
		else // skip blind alley
			--n;
	}

	node->CN.f[function].funcMin(functions, n);

	delete [] functions;
}


// can be performed on node with all blind alleys
template <class Game>
void CNSearch<Game>::updateNode(struct CNSearch<Game>::Tree * node, int depth)
{
	bool hasSon = false;
	for(unsigned int i = 0; i < node->moves.size(); ++i)
		if(node->moves[i] != NULL)
		{
			hasSon = true;
			break;
		}
	assert(!PASS_ALLOWED || node->moves.size() > 0);

	if(!hasSon)
	{
		node->moves.clear();
		node->CN.f[PROOF_FUNC].init(Game::MIN_SCORE, UINT_MAX / 2);
		node->CN.f[DISPROOF_FUNC].init(-Game::MIN_SCORE, UINT_MAX / 2);
		node->value = Game::MIN_SCORE;
	}
	else
	{
		if(firstPlayer(depth))
		{
			updateSum(node, DISPROOF_FUNC);
			updateMin(node, PROOF_FUNC);
		}
		else
		{
			updateSum(node, PROOF_FUNC);
			updateMin(node, DISPROOF_FUNC);
		}

		node->CN.visits = 1;
		node->nodesExpanded = 1;
		for(unsigned int i = 0; i < node->moves.size(); ++i)
		{
			if(node->moves[i] != NULL)
			{
				node->CN.visits += node->moves[i]->CN.visits;
				node->nodesExpanded += node->moves[i]->nodesExpanded;
			}
		}

		node->value = node->CN.f[PROOF_FUNC].lastZeroArg();
		assert(node->value == -1. * (node->CN.f[DISPROOF_FUNC].lastZeroArg()));

		// diagnostic info, very slow :(
		if(debug)
		{
			assert(node->CN.f[PROOF_FUNC].isMonotonious());
			assert(node->CN.f[DISPROOF_FUNC].isMonotonious());

			// also check if value is correct min-max value
			if(firstPlayer(depth)) // max node
			{
				for(unsigned int i = 0; i < node->moves.size(); ++i)
					if(node->moves[i] != NULL)
						assert(node->value >= node->moves[i]->value);
			}
			else
			{
				for(unsigned int i = 0; i < node->moves.size(); ++i)
				{
					if(node->moves[i] != NULL)
					{
						assert(node->value <= node->moves[i]->value);
					}
				}
			}
		}
	}
}

template <class Game>
inline bool CNSearch<Game>::Tree::isLeaf()
{
	return this->moves.size() == 0;
}

template <class Game>
inline bool CNSearch<Game>::firstSmallMove(int depth)
{
	return depth % Game::MOVES_PER_PLAYER == 0;
}

template <class Game>
inline bool CNSearch<Game>::lastSmallMove(int depth)
{
	return (depth + 1) % Game::MOVES_PER_PLAYER == 0;
}

template <class Game>
inline bool CNSearch<Game>::firstPlayer(int depth)
{
	return depth / Game::MOVES_PER_PLAYER % 2 == 0;
}

template <class Game>
inline bool CNSearch<Game>::blindAlley(struct CNSearch<Game>::Tree * node)
{
	return node == NULL;
}

template <class Game>
typename Game::EvalT CNSearch<Game>::evalPosition(struct Tree * tree,
		unsigned int iterations, unsigned int movesMade)
{
	hits = 0;
	misses = 0;
	lostIterations = 0;

	for(; iterations > 0; --iterations)
	{
		assert(tree != NULL);
		if(tree->CN.f[PROOF_FUNC].firstNonzeroVal() >= UINT_MAX / 2 &&
				tree->CN.f[DISPROOF_FUNC].firstNonzeroVal() >= UINT_MAX / 2)
		{
			break;
		}

		EvalT pfArg = tree->CN.f[PROOF_FUNC].argFor(C),
					dfArg = tree->CN.f[DISPROOF_FUNC].argFor(C);
		assert(pfArg != Game::INF_SCORE && dfArg != Game::INF_SCORE);
		assert(tree->CN.f[PROOF_FUNC].valAt(pfArg) < UINT_MAX / 2);
		assert(tree->CN.f[DISPROOF_FUNC].valAt(dfArg) < UINT_MAX / 2);
		assert(history.find(game->stateHash()) == history.end());
		if(firstPlayer(movesMade))
			search(tree, pfArg, dfArg, PROOF_FUNC, movesMade);
		else
			search(tree, dfArg, pfArg, DISPROOF_FUNC, movesMade);
	}

	cerr << "Position evaluated, TT hits: " << hits << ", misses: " << misses <<
		", visits: " << tree->CN.visits << "; nodes: " << tree->nodesExpanded << 
		", lostIters: " << lostIterations << endl;

	return tree->value;
}

template <class Game>
typename Game::EvalT CNSearch<Game>::evalPositionDFS(struct Tree * tree,
		unsigned int iterations, unsigned int movesMade)
{
	assert(!tree->isLeaf());
	hits = 0;
	misses = 0;
	lostIterations = 0;

	for(int i = iterations; i > 0;)
	{
		assert(tree != NULL);
		if(tree->CN.f[PROOF_FUNC].firstNonzeroVal() >= UINT_MAX / 2 &&
				tree->CN.f[DISPROOF_FUNC].firstNonzeroVal() >= UINT_MAX / 2)
		{
			break;
		}

		EvalT pfArg = tree->CN.f[PROOF_FUNC].argFor(C),
					dfArg = tree->CN.f[DISPROOF_FUNC].argFor(C);

		assert(pfArg != Game::INF_SCORE && dfArg != Game::INF_SCORE);
		assert(tree->CN.f[PROOF_FUNC].valAt(pfArg) < UINT_MAX / 2);
		assert(tree->CN.f[DISPROOF_FUNC].valAt(dfArg) < UINT_MAX / 2);

		unsigned int pfTH = tree->CN.f[PROOF_FUNC].valAt(pfArg) + 1,
				dfTH = tree->CN.f[DISPROOF_FUNC].valAt(dfArg) + 1;

		if(firstPlayer(movesMade))
			i -= searchDFS(tree, pfArg, dfArg, pfTH, dfTH,
					PROOF_FUNC, movesMade, max(i, MAX_DFS_ITER));
		else
			i -= searchDFS(tree, dfArg, pfArg, dfTH, pfTH,
					DISPROOF_FUNC, movesMade, max(i, MAX_DFS_ITER));

		if(i > 0)
		{
			assert(pfArg != Game::INF_SCORE && dfArg != Game::INF_SCORE);
			if(!(pfTH <= tree->CN.f[PROOF_FUNC].valAt(pfArg) ||
						dfTH <= tree->CN.f[DISPROOF_FUNC].valAt(dfArg)))
			{
				cerr << i << endl;
				cerr << pfTH << endl;
				cerr << dfTH << endl;
				printLog(tree, 0);
			}
			assert(pfTH <= tree->CN.f[PROOF_FUNC].valAt(pfArg) ||
					dfTH <= tree->CN.f[DISPROOF_FUNC].valAt(dfArg));
		}

	}
	cerr << "Position evaluated, TT hits: " << hits << ", misses: " << misses << 
		", visits: " << tree->CN.visits << ", nodes: " << tree->nodesExpanded << endl;

	EvalT posVal = tree->value;
	return posVal;
}

template <class Game>
typename Game::MoveT * CNSearch<Game>::bestMove(unsigned int iterations)
{
	struct Tree * tree = newTree(Game::DRAW_SCORE);
	assert(!game->endOfGame());
	developNode(tree, 0);
	updateNode(tree, 0);
	struct Tree * node = tree;
	if(STATES_REPEAT)
	{
		assert(firstSmallMove(0));
		history.insert(game->stateHash());
	}
	unsigned int m;

	for(m = 0; m < Game::MOVES_PER_PLAYER; ++m)
	{
		if(game->endOfGame())
		{
			for(unsigned int mm = m; mm < Game::MOVES_PER_PLAYER; ++mm)
			{
				assert(mm > 0);
				//repeated moves are not executed in diaballik
				moves[mm] = moves[mm - 1];
			}
			break;
		}
		else if(node->isLeaf())
		{
			// we expect the node that we enter
			// to be developed
			// assert(false)
			developNode(node, m);
			updateNode(node, 0);
		}


		EvalT maxGameVal = -Game::INF_SCORE;
		MoveT const * chosenMove = NULL;
		unsigned idx = 0;

		if(PASS_ALLOWED && !game->canMove())
		{
			assert(false);
			maxGameVal = node->moves[0]->value;
			chosenMove = game->getPass();
			moves[m] = *chosenMove;
			game->makePass();
		}
		else
		{
			unsigned int i = 0;
			MoveListT * moveList = game->getMoveList();
			assert(node->moves.size() > 0);
			for(MoveT * mv = game->getFirstMove(moveList); mv; mv = game->getNextMove(moveList), ++i)
			{
				if(PASS_ALLOWED || !blindAlley(node->moves[i]))
				{
					assert(i < node->moves.size());
					game->makeMove(mv);

					EvalT val;
					if(game->endOfGame())
					{
						if(lastSmallMove(m))
							val = -game->eval();
						else
							val = game->eval();
					}
					else
					{
#ifdef DFS
					if(node->moves[i]->isLeaf())
					{
						developNode(node->moves[i], m + 1);
						updateNode(node->moves[i], m + 1);
					}
					if(m + 1 == Game::MOVES_PER_PLAYER &&
							history.find(game->stateHash()) != history.end())
						val = Game::DRAW_SCORE;
					else
						val = evalPositionDFS(node->moves[i], iterations, m + 1);
#else
					if(m + 1 == Game::MOVES_PER_PLAYER &&
							history.find(game->stateHash()) != history.end())
						val = Game::DRAW_SCORE;
					else
						val = evalPosition(node->moves[i], iterations, m + 1);
#endif
					}

					assert(val > -Game::INF_SCORE);

					if(/*(!node->moves[i]->isLeaf() || game->endOfGame()) &&*/ val > maxGameVal)
					{
						maxGameVal = val;
						chosenMove = mv;
						idx = i;
					}
					game->undoMove();
				}
			}

			assert(maxGameVal > -Game::INF_SCORE);

			moves[m] = *chosenMove;
			game->makeMove(chosenMove);
			game->rmMoveList(moveList);
		}
		node = node->moves[idx];
	}

	for(; m > 0; --m)
		game->undoMove();

	if(STATES_REPEAT)
	{
		assert(history.find(game->stateHash()) != history.end());
		history.erase(game->stateHash());
	}

	cleanTree(tree);

	return moves;
}

template <class Game>
void CNSearch<Game>::search(struct CNSearch<Game>::Tree * node,
		EvalT currArg, EvalT sndArg, int currFunc, int depth)
{
	assert(node != NULL);
	assert(node->CN.f[PROOF_FUNC].firstNonzeroVal() < UINT_MAX / 2 ||
			node->CN.f[DISPROOF_FUNC].firstNonzeroVal() < UINT_MAX / 2);

	assert(currArg != Game::INF_SCORE && sndArg != Game::INF_SCORE);

	assert(node->CN.f[currFunc].valAt(currArg) > 0 ||
			node->CN.f[1 - currFunc].valAt(sndArg) > 0);
	assert(node->CN.f[currFunc].valAt(currArg) < UINT_MAX / 2);
	assert(node->CN.f[1 - currFunc].valAt(sndArg) < UINT_MAX / 2);

	if(node->isLeaf())
		searchLeaf(node, depth);
	else
		searchInternalNode(node, currArg, sndArg, currFunc, depth);
}

template <class Game>
void CNSearch<Game>::searchLeaf(struct CNSearch<Game>::Tree * node, int depth)
{
	assert(!game->endOfGame());
	if(!USE_TT)
	{
		assert(node->CN.f[PROOF_FUNC].firstNonzeroVal() == 1);
		assert(node->CN.f[DISPROOF_FUNC].firstNonzeroVal() == 1);
	}

	if(STATES_REPEAT && history.find(game->stateHash()) != history.end())
	{
		node->value = Game::DRAW_SCORE;
		node->CN.visits = 1;
		node->nodesExpanded = 1;
		node->hasRef = false;
	}
	else
	{
		assert(!game->endOfGame());
		developNode(node, depth);
		// if pass is allowed, it should be in moves list
		assert(!PASS_ALLOWED || node->moves.size() > 0);
		if(node->isLeaf())
			++lostIterations;
		updateNode(node, depth);
	}
}

template <class Game>
void CNSearch<Game>::searchInternalNode(struct CNSearch<Game>::Tree * node,
		EvalT currArg, EvalT sndArg, int currFunc, int depth)
{
	if(PASS_ALLOWED && !game->canMove())
	{
		if(STATES_REPEAT && firstSmallMove(depth))
			history.insert(game->stateHash());
		game->makePass();

		assert(history.find(game->stateHash()) == history.end());
		if(lastSmallMove(depth))
			search(node->moves[0], sndArg, currArg, 1 - currFunc, depth + 1);
		else
			search(node->moves[0], currArg, sndArg, currFunc, depth + 1);


		game->undoPass();
		if(STATES_REPEAT && firstSmallMove(depth))
		{
			assert(history.find(game->stateHash()) != history.end());
			history.erase(game->stateHash());
		}

		updateNode(node, depth);
	}
	else
	{
		unsigned int idx = 1000;
		MoveT * chosenMove = NULL;

		MoveListT * moveList = game->getMoveList();
		chooseMove(node, currArg, currFunc, moveList, idx, chosenMove);
		assert(idx != 1000 || chosenMove == NULL);

		if(!PASS_ALLOWED && chosenMove == NULL)
		{
			// i don't really know if it should happen
			// assert(false)
			node->moves.clear(); // this will tell our parent that we aren't worthy
			node->CN.f[PROOF_FUNC].init(Game::MIN_SCORE, UINT_MAX / 2);
			node->CN.f[DISPROOF_FUNC].init(-Game::MIN_SCORE, UINT_MAX / 2);
			node->value = Game::MIN_SCORE;
		}
		else
		{
			if(STATES_REPEAT && firstSmallMove(depth))
				history.insert(game->stateHash());
			game->makeMove(chosenMove);

			if(lastSmallMove(depth))
				search(node->moves[idx], sndArg, currArg, 1 - currFunc, depth + 1);
			else
				search(node->moves[idx], currArg, sndArg, currFunc, depth + 1);

			game->undoMove();
			if(STATES_REPEAT && firstSmallMove(depth))
			{
				assert(history.find(game->stateHash()) != history.end());
				history.erase(game->stateHash());
			}

			// Possibly recursive search limited the count of moves from node
			// node->moves[minPtr]. We have to check if this count is still > 0
			if(!PASS_ALLOWED && node->moves[idx]->moves.size() == 0)
			{
				cleanTree(node->moves[idx]);
				node->moves[idx] = NULL;
			}

			updateNode(node, depth);
		}
		game->rmMoveList(moveList);
	}
	assert(node->CN.visits > 0);
	if(USE_TT)
	{
		hashTable.save(game->stateHash(), node->CN, node->CN.visits);
	}
}

template <class Game>
void CNSearch<Game>::chooseMove(struct CNSearch<Game>::Tree * node,
		EvalT currArg, int currFunc, MoveListT * moveList,
		unsigned int &idx, MoveT * &chosenMove)
{
	unsigned int minVal = UINT_MAX;
	unsigned int i = 0;
	for(MoveT * mv = game->getFirstMove(moveList); mv; mv = game->getNextMove(moveList), ++i)
	{
		assert(i < node->moves.size());
		if(PASS_ALLOWED || !blindAlley(node->moves[i]))
		{
			assert(currArg != Game::INF_SCORE);
			unsigned int currVal = node->moves[i]->CN.f[currFunc].valAt(currArg);
			if(currVal < minVal)
			{
				minVal = currVal;
				idx = i;
				chosenMove = mv;
			}
		}
	}
	if(minVal == UINT_MAX / 2)
	{
		cerr << currArg  << endl;
		cerr << currFunc << endl;
		printLog(node, 0);
	}
	assert(minVal != UINT_MAX / 2);
	if(minVal == UINT_MAX)
	{
		assert(false);
		assert(!PASS_ALLOWED);
		++lostIterations;
	}
}

template <class Game>
int CNSearch<Game>::searchDFS(struct CNSearch<Game>::Tree * &node,
		EvalT currArg, EvalT sndArg, unsigned int currTH, unsigned int sndTH,
		int currFunc, int depth, int iterations)
{
	assert(node != NULL);
	assert(depth <= 1000);
	int iterationsDone = 0;

	assert(node->CN.f[currFunc].valAt(currArg) > 0 ||
			node->CN.f[1 - currFunc].valAt(sndArg) > 0);

	// end of search
	if(node->CN.f[currFunc].valAt(currArg) >= currTH ||
			node->CN.f[1 - currFunc].valAt(sndArg) >= sndTH)
	{
		++iterationsDone;
	}
	else
	{
		while(iterationsDone < iterations &&
				node->CN.f[currFunc].valAt(currArg) < currTH &&
				node->CN.f[1 - currFunc].valAt(sndArg) < sndTH)
		{
			assert(node->CN.f[currFunc].valAt(currArg) < UINT_MAX / 2);
			assert(node->CN.f[1 - currFunc].valAt(sndArg) < UINT_MAX / 2);

			iterationsDone += searchDeeper(node, currArg, sndArg, currTH, sndTH,
					currFunc, depth, iterations - iterationsDone);

			if(blindAlley(node))
			{
				return iterationsDone;
			}
		}
		if(iterationsDone < iterations)
			assert(currTH <= node->CN.f[currFunc].valAt(currArg) ||
					sndTH <= node->CN.f[1 - currFunc].valAt(sndArg));
	}
	assert(iterationsDone <= iterations);

	if(USE_TT)
	{
		hashTable.save(game->stateHash(), node->CN, node->CN.visits);
	}

	return iterationsDone;
}

template <class Game>
int CNSearch<Game>::searchDeeper(struct CNSearch<Game>::Tree * &node,
		EvalT currArg, EvalT sndArg, unsigned int currTH, unsigned int sndTH,
		int currFunc, int depth, int iterations)
{
	assert(node != NULL);
	int iterationsDone = 0;
	if(PASS_ALLOWED && !game->canMove())
	{
		iterationsDone +=
			passDeeper(node, currArg, sndArg, currTH, sndTH, currFunc, depth, iterations);
	}
	else
	{
		unsigned int minVal1 = UINT_MAX, minVal2 = UINT_MAX;
		unsigned int minPtr1 = 0;
		unsigned int i = 0;
		MoveT * chosenMove = NULL;

		MoveListT * moveList = game->getMoveList();

		// find move to search
		for(MoveT * mv = game->getFirstMove(moveList); mv; mv = game->getNextMove(moveList), ++i)
		{
			assert(i < node->moves.size());
			if(PASS_ALLOWED || !blindAlley(node->moves[i]))
			{
				unsigned int currVal = node->moves[i]->CN.f[currFunc].valAt(currArg);
				if(currVal < minVal1)
				{
					minVal2 = minVal1;
					minVal1 = currVal;
					minPtr1 = i;
					chosenMove = mv;
				}
				else if(currVal < minVal2)
				{
					minVal2 = currVal;
				}
			}
		}

		if(!PASS_ALLOWED && minVal1 == UINT_MAX)
		{
			// blind alley
			cleanTree(node);
			node = NULL;
		}
		else
		{
			unsigned int newCurrTH, newSndTH;
			if(minVal2 == UINT_MAX)
				newCurrTH = currTH;
			else
				newCurrTH = min(currTH, minVal2 + 1);// (1 + epsilon trick?)

			newSndTH = sndTH - node->CN.f[1 - currFunc].valAt(sndArg) +
				node->moves[minPtr1]->CN.f[1 - currFunc].valAt(sndArg);

			// search further
			assert(chosenMove != NULL);
			if(STATES_REPEAT && firstSmallMove(depth))
				history.insert(game->stateHash());

			game->makeMove(chosenMove);
			assert(!game->endOfGame());

			assert(!game->endOfGame());
			developNode(node->moves[minPtr1], depth + 1);
			updateNode(node->moves[minPtr1], depth + 1);

			if(STATES_REPEAT && firstSmallMove(depth+1) && history.find(game->stateHash()) != history.end())
			{
			}
			else
			{
				if(lastSmallMove(depth))
					iterationsDone += searchDFS(node->moves[minPtr1], sndArg, currArg,
							newSndTH, newCurrTH, 1 - currFunc, depth + 1, iterations);
				else
					iterationsDone += searchDFS(node->moves[minPtr1], currArg, sndArg,
							newCurrTH, newSndTH, currFunc, depth + 1, iterations);
			}

			assert(blindAlley(node->moves[minPtr1]) || node->moves[minPtr1]->CN.visits > 0);
			game->undoMove();
			if(STATES_REPEAT && firstSmallMove(depth))
			{
				assert(history.find(game->stateHash()) != history.end());
				history.erase(game->stateHash());
			}

			updateNode(node, depth);

			if(!STATES_REPEAT || !blindAlley(node->moves[minPtr1]))
			{
				for(unsigned int i = 0; i < node->moves[minPtr1]->moves.size(); ++i)
				{
					if(!blindAlley(node->moves[minPtr1]->moves[i]))
						cleanTree(node->moves[minPtr1]->moves[i]);
				}
				node->moves[minPtr1]->moves.clear();
			}
		}
		game->rmMoveList(moveList);
	}
	return iterationsDone;
}

template <class Game>
int CNSearch<Game>::passDeeper(struct CNSearch<Game>::Tree * &node,
		EvalT currArg, EvalT sndArg, unsigned int currTH, unsigned int sndTH,
		int currFunc, int depth, int iterations)
{
	assert(node != NULL);
	int iterationsDone = 0;
	if(STATES_REPEAT && firstSmallMove(depth))
		history.insert(game->stateHash());
	game->makePass();

	assert(node->moves.size() == 1);
	assert(!game->endOfGame());
	developNode(node->moves[0], depth + 1);
	updateNode(node->moves[0], depth + 1);

	if(lastSmallMove(depth))
		iterationsDone += searchDFS(node->moves[0], sndArg, currArg,
				sndTH, currTH, 1 - currFunc, depth + 1, iterations);
	else
		iterationsDone += searchDFS(node->moves[0], currArg, sndArg,
				currTH, sndTH, currFunc, depth + 1, iterations);

	game->undoPass();
	if(STATES_REPEAT && firstSmallMove(depth))
	{
		assert(history.find(game->stateHash()) != history.end());
		history.erase(game->stateHash());
	}

	updateNode(node, depth);
	for(unsigned int i = 0; i < node->moves[0]->moves.size(); ++i)
	{
		cleanTree(node->moves[0]->moves[i]);
	}
	node->moves[0]->moves.clear();

	return iterationsDone;
}

template <class Game>
void CNSearch<Game>::printLog(struct CNSearch<Game>::Tree * node, int depth)
{
	if(depth < 4)
	{
		// Print current node!
		for(int i = 0; i < depth; ++i)
			cerr << " -- ";
		cerr << "> value = " << node->value << "; depth = " << depth <<
			"; visits = " << node->CN.visits << "; nodes = " <<
			node->nodesExpanded << endl;
		for(int i = 0; i < depth; ++i)
			cerr << "    ";
		cerr << "  ProofFunction: ";
		node->CN.f[PROOF_FUNC].print();
		for(int i = 0; i < depth; ++i)
			cerr << "    ";
		cerr << "  DisproofFunction: ";
		node->CN.f[DISPROOF_FUNC].print();

		// Recursively print all children
		for(unsigned int i = 0; i < node->moves.size(); ++i)
		{
			if(node->moves[i] == NULL) // skip blind alleys
			{
				for(int i = 0; i < depth + 1; ++i)
					cerr << " -- ";
				cerr << "> BLIND ALLEY" << endl;
			}
			else
			{
				printLog(node->moves[i], depth + 1);
			}
		}
	}
}

#endif
