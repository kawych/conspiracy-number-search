#ifndef __TRANSPOSITION_TABLE__
#define __TRANSPOSITION_TABLE__

#include <cstring>
#include <cstdlib>
#include <iostream>
#include <stdint.h>

using namespace std;

const unsigned int SEARCH = 4;
const uint32_t HASHTAB_SIZE = 1024 * 1024 * 2;
const uint64_t IDX_MASK = ~((uint64_t) 0) >> (64 - 21);

static inline int IDX(uint64_t hash) {
	return (hash & IDX_MASK);
}

template <typename HashT, typename EntryT>
class TranspositionTable {
	public:
		TranspositionTable();
		~TranspositionTable();
		EntryT * find(HashT hash);
		// save overwrites the same entry (identified by hash)
		// with smaller value (smaller value has higher priority)
		void save(HashT h, const EntryT &e, uint32_t v);
		void erase(int idx);
		void clear();
	private:
		HashT * hash;
		EntryT * entry;
		uint32_t * value;
};

template <typename HashT, typename EntryT>
TranspositionTable<HashT, EntryT>::TranspositionTable() {
	hash = new HashT[HASHTAB_SIZE];
	entry = new EntryT[HASHTAB_SIZE];
	value = new uint32_t[HASHTAB_SIZE];
	memset(hash, 0, sizeof(HashT) * HASHTAB_SIZE);
	memset(entry, 0, sizeof(EntryT *) * HASHTAB_SIZE);
}

template <typename HashT, typename EntryT>
TranspositionTable<HashT, EntryT>::~TranspositionTable() {
	//for(unsigned int i = 0; i < HASHTAB_SIZE; ++i)
	//	delete entry[i];
	delete[] hash;
	delete[] entry;
	delete[] value;
}

template <typename HashT, typename EntryT>
inline EntryT * TranspositionTable<HashT, EntryT>::find(HashT h){
	assert(h != 0);
	for(unsigned int i = 0; i < SEARCH; ++i) {
		if(hash[IDX(IDX((uint64_t) h) + i)] == h) {
			//cerr << "hit: " << value[IDX(IDX((uint64_t) h) + i)] << " <" << h << ">" << endl;
			return & entry[IDX(IDX((uint64_t) h) + i)];
		}
	}
	//cerr << "miss" << endl;
	return NULL;
}

template <typename HashT, typename EntryT>
void TranspositionTable<HashT, EntryT>::save(HashT h, const EntryT &e, uint32_t v) {
	assert(h != 0);
	int idx = -1;
	uint32_t best = UINT_MAX;
	for(unsigned int i = 0; i < SEARCH; ++i) {
		if(hash[IDX(IDX((uint64_t) h) + i)] == 0) {
			idx = IDX(IDX((uint64_t) h) + i);
			best = value[idx];
			break;
		}
		else if(hash[IDX(IDX((uint64_t) h) + i)] ==  h) {
			if(v > value[IDX(IDX((uint64_t) h) + i)])
			{
				//cerr << "savetza " << v << " over " << value[IDX(IDX((uint64_t) h) + i)] << endl;
				idx = IDX(IDX((uint64_t) h) + i);
				break;
			}
			else
			{
				//cerr << "unsavatto <" << h << "> (" << v << ") coz (" <<
				//	value[IDX(IDX((uint64_t) h) + i)] << ")" << endl;
				//cerr << "blahizza " << v << " bunder " << value[IDX(IDX((uint64_t) h) + i)] << endl;
				return;
			}
		}
		else if(value[IDX(IDX((uint64_t) h) + i)] < best) {
			idx = IDX(IDX((uint64_t) h) + i);
			best = value[idx];
		}
	}
	//cerr << "save <" << h << "> (" << v << ")" << endl;
	assert(idx != -1);
	//delete entry[idx];
	hash[idx] = h;
	entry[idx] = e;
	value[idx] = v;
	//cerr << "save: " << v << " <" << h << ">" << endl;
}

template <typename HashT, typename EntryT>
inline void TranspositionTable<HashT, EntryT>::erase(int idx) {
	//delete entry[idx];
	hash[idx] = 0;
	entry[idx] = NULL;
}

template <typename HashT, typename EntryT>
inline void TranspositionTable<HashT, EntryT>::clear() {
	//for(unsigned int i = 0; i < HASHTAB_SIZE; ++i)
	//	delete entry[i];
	memset(hash, 0, sizeof(HashT) * HASHTAB_SIZE);
	memset(entry, 0, sizeof(EntryT) * HASHTAB_SIZE);
}

#endif
